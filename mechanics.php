<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<div style="height:100%; width:100%; text-align:center;">
    <img src="images/closebutton.png" alt="" style="cursor: pointer; margin-left: 680px;" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" />
  <h2>THE SWEEPS CENTER OFFICIAL RULES AND MECHANICS</h2>
  <br />
  <br />
<div style="text-align:justify; margin-left:auto; margin-right:auto;">
    <ol type="I" style="font-size:1.2em;">
      <li style="font-weight:bold;">Sweeps Center Points of Entry </li>
      <br />
      The Sweeps Center shall give the participant two ways to get sweepstakes entries: (a) via Free Entry Service; and (b) via Product Purchase. Participants need to visit any of the Sweeps Center branches in order to participate in the sweepstakes. 
      <br /><br />
        <ol type="A">
          <li><b>To participate through the Free Entry Service</b>, the Participant must first submit a registration. Registration can be done in two ways - via Online Registration and Sweeps Center Free Entry Terminal.
              <br /><br />
              <ol type="a">
                  <li>To register online, Participant must go to www.thesweepscenter.com and fill in the form with his full name, home number and/or mobile number, mailing address and e-mail address.</li><br />
                  <li>To register through the Free Entry Terminal, Participant must visit a Sweeps Center branch and ask the Cashier or Sweeps Assistant for assistance to register on the Free Entry service. Sweeps Assistant shall direct the Participant to the Free Entry Terminal where he shall fill in the same form provided in the online registration.</li><br />
              </ol>
              <br />
              <ol type="i">
                  <li>The Participant shall be notified instantly if his registration, either through online service or the Sweeps Center Free entry terminal, qualifies for a free entry. After successful registration, the Sweeps Center System shall send a Sweeps code to the Participant's registered e-mail address after the submission of the registration. Successful registration in this case means submitted registration details passed The Sweeps Center's system validations, which include checking of the submitted details and fulfillment of the daily free entry limit.</li><br />
                  <li>Participation via the Free Entry service shall be limited to only one (1) registration per person per day. A "day" is defined as the 24 hours between 12:00:00 am (midnight) and 11:59:59 pm. </li><br />
                  <li>All registration information shall become property of Guam Sweepstakes Corporation.</li><br />
                  <li>Once the Sweeps code has been received by the Participant, he/she must visit any of the Sweeps Center branches so he/she can avail his/her free sweepstakes entry for the day. The Participant must enter his/her Sweeps code by using any of the available terminals inside the center. The Participant shall get one (1) free electronic sweepstakes entry for every valid free entry Sweeps code. The Sweeps Code should be existing, unused and unexpired upon entry on the terminals. The Participant must enter this free entry Sweeps code within three days after the code has been sent; otherwise the code will expire and lose validity.</li><br />
		    <li>The Sweeps Center has the right to invalidate free entry codes which are results of Multiple Registrations.</li><br />
              </ol>
          </li>
          <li><b>To participate through product purchase</b>, the Participant must purchase the Sweeps Center's Internet Browsing Pre-paid card and enter the card PIN on any of the Launch Pad terminals inside the Sweeps Center. The value of his card will be the equivalent number of points which will be loaded to his chosen terminal.</li>
          <br /><br />
              <ol type="a">
                  <li>The loaded points in the terminal shall be the credits used by the Participant to avail of the Internet Browsing service offered by the Sweeps Center. Internet browsing is 10 points per hour. Charging shall be on a per half an hour basis. An hour in this case starts on the current hour and ends at the beginning of the next hour. A half hour is each thirty (30) minute segment that comprises one (1) hour. Five (5) points shall be deducted from the Participant's point balance for every half hour block that he browses the Internet. </li><br />
                  <li>Every purchase of a card guarantees the Participant at least one sweepstakes card at the end of his session. </li><br />
                  <li>The Participant can use up all or part of his points for Internet browsing. However, the number of the e-Sweepstakes entries that the Participant will get at the end of his session will be dependent on what will be left on his points balance. <i>(See Section III for Points Conversion to Electronic Sweepstakes)</i></li><br />              
              </ol>
        </ol>
      <br />
      <li style="font-weight:bold;">How to Accumulate/Increase Your Points</li>
      <br />
      The participant can increase the number of sweepstakes entries he/she can get from the Sweeps Center by increasing his/her number of points. There are two ways to increase/accumulate the participant's points:
      <br /><br />
      <ol type="a">
          <li><b>Sweeps Game Play</b><br /><br />
              <ol type="i">
                  <li>The participant can accumulate more points to be converted into electronic sweepstakes entries by playing the Sweeps Center's games such as Horse Racing, Bingo & Slots. To play, participant must choose to Play Games instead of Browse the Internet on the Sweeps Center Terminal's interface. Winnings in the games that the participant played will be automatically added to his/her point balance. </li><br />
                  <li>After game play, the participant must convert his point balance to get his sweepstakes entries. Every one (1) point remaining in the participant's balance shall be converted into one (1) electronic sweepstakes entry. <i>(See Section III for Points Conversion to Electronic Sweepstakes)</i></li><br />
              </ol> 
          </li>
          <li><b>Purchase of More Sweeps Center Products</b><br /><br />
              <ol type="i">
                  <li>The participant can increase his/her points through purchase of the Sweeps Center's Internet Card. </li><br />
                  <li>To purchase Internet Browsing time, the participant must go to the cashier or ask the Sweeps Assistant inside the Sweeps Center for a purchase of the Internet Card.</li><br />
                  <li>To increase the current point balance, the participant must then enter the card PIN on his terminal to credit the equivalent value of points to his/her terminal. <i>(See Section I, Item B, for The Sweeps Center Points of Entry)</i></li><br />
              </ol>
          </li>
      </ol>
      <br />
      <li style="font-weight:bold;">Points Conversion to Electronic Sweepstakes </li>
      <br />
      Conversion of points can be done after any of the following:
      <br /><br />
      <ol type="a">
          <li>Loading of points through the use of the Internet Card PIN</li><br />
          <li>End of Internet Browsing Session</li><br />
          <li>End of Sweeps Game Play</li><br />
          <li>Successful entry of a Free Entry Sweeps Code</li><br />
      </ol>
      <br />
      <ol type="i">
          <li>To convert points, the participant must click the CONVERT POINTS button on the terminal's interface. Each point in the participant's point balance shall be converted into one (1) electronic sweepstakes entry.</li><br />
          <li>Decimals in the participant's point balance shall be rounder down.</li><br />
          <li>In cases where the participant has consumed all his points for Internet browsing or has lost all his points from game play, the zero (0) point balance must still be converted. This shall reward the player with one (1) electronic sweepstakes entry.</li><br />
          <li>Successful entry of Sweeps code from Free Entry service shall credit .10 points to the terminal's point balance. These .10 points must be converted in order for the participant to get his one (1) free sweepstakes entry. </li><br />
          <li>Upon successful conversion, a selection of sweepstakes cards will be displayed for the participant. The participant is allowed to choose only the number of cards equivalent from his/her points.</li><br />
      </ol> 
      <br />
      <li style="font-weight:bold;">Prizes and Claiming Procedures</li>
      <br />
      <ol type="a">
          <li>The participant with a winning sweepstakes entry shall be notified of the entry's value at the time of participation.</li><br />
          <li>All winnings must be within the day of the participation at the Sweeps Center's cashier. Any winnings unclaimed within the day will be forfeited. Unclaimed prizes will not be awarded.</li><br /><br />
          <li>To claim prizes/winnings, the participant must proceed to the cashier and hand over the terminal name plate and the transaction reference number, which will be displayed together with the notification of winnings on the terminal's interface. Upon validation that there are winnings on the terminal, cashier shall process the winnings and hand over the prize/s to the participant. In the event that the amount of winnings of the participant is bigger that what the cashier currently has on his/her drawer, the participant shall wait in the Sweeps Center, while the cashier.s request from Sweeps Center's Head Office, for additional amount of prize to be paid to the participant is being delivered.</li><br />
          <li>Any prize provided by the Sweeps Center for any kind of merchandise or cash is not inclusive of any taxes, registrations, licensing, insurance, postage or delivery unless otherwise stipulated by Guam Sweepstakes Corporation. Winner shall be solely responsible for all state, federal and local taxes on the prize. </li><br />
          <li>No transfer, refund, substitution or replacement of prizes is permitted except that Guam Sweepstakes Corporation reserves the right in its sole discretion to substitute a prize or equal or greater value or cash equivalent.</li><br />
          <li>If winner is disqualified for any reason, prizes will be forfeited.</li><br />
      </ul>
    </ol>
      <img src="images/closebutton.png" alt="" style="cursor: pointer; margin-left: 560px; float: left;" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" /><div style="cursor: pointer; margin-left: 550px; margin-top: 5px;" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"><b>CLOSE</b></div>
      <br/>
</div>
</div>
</html>

