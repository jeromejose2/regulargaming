<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<script src="js/form_validation.js" type="text/javascript"></script>
    <div style="height:100%; width:100%; text-align:center;">
        <!--<img src="images/closebutton.png" alt="" style="cursor: pointer; margin-left: 90%;" onclick="document.getElementById('light3').style.display='none';document.getElementById('fade').style.display='none'" />-->
        <h2><img src="images/theSweepsLogo.png" style="height: 120px;" /></h2>
        <br />
        <br />
        <h3><u>Terms & Conditions</u></h3>
        <br/>
        <br/>
        <div align="center">
        <div style="text-align:justify; margin-left:20px;">
            <ul style="font-size:1.2em; line-height:1; list-style-type:inherit">
              <li>The Sweeps Center is intended for entry in Guam territory only and shall be governed by U.S. Law.</li>
              <br />
              <li>These terms & conditions constitute a legally binding agreement between <b>Guam Sweepstakes Corporation</b> and <b>The Participant</b> of this sweeps service operating under the brand name, <b>"The Sweeps Center."</b> By participating and using the services offered by The Sweeps Center, the Participant is agreeing to be bound by the following terms and conditions, the rules of any game the Participant plays in the Sweeps games and the terms of the coupons to be exchanged for products at participating establishments.</li>
              <br/>
              <li>The Participant fully understands, agrees to and shall adhere to all the rules and regulations, terms and conditions, presented herein and as such rules, regulations, terms and conditions may change from time to time.</li>
              <br/>
              <li>The Participant guarantees that he is 18 years of age or older and that all information he provides to The Sweeps Center concerning him is true, complete and accurate. The Sweeps Center and Guam Sweepstakes Corporation reserve the right to request proof of identification and age at any time to ensure the prohibition of participation of minors. The Participant represents and warrants that he is not one of the officers, directors, employees, consultants or agents or he is not one in the same manner connected to one of the affiliated or subsidiary companies, suppliers, vendors or sponsors, and he is not a relative of any of them (the term relative referring to the spouse, domestic partner, parent, child or sibling) of Guam Sweepstakes Corporation, Philweb Asia-Pacific Corporation, PhilWeb Corporation or The Sweeps Center. In the event that the Participant violates this restriction, he may lose rights to any prizes he might have won from using The Sweeps Center services.</li>
              <br/>
              <li>The Participant shall use the Sweeps Center for his personal entertainment and not for any business or commercial purpose. </li>
              <br/>
              <li><b>No purchase is needed</b> in direct exchange of an electronic sweepstake card. Payments of any kind will not increase your chances of winning in the sweepstakes. </li>
              <br/>  
              <li>The Sweeps Center shall give the Participant sweepstakes entries through virtual sweepstakes cards with instant cash prizes with various denominations.</li>
              <br/>              
              <li>The Sweeps Center shall give the Participant two ways to get sweepstakes entries - via Free Entry Service and via Purchase of Products offered in The Sweeps Center. Participants need to visit any of the Sweeps Center branches in order to participate in the sweepstakes.</li>
              <br/>
              
              <li>
                 <b>To participate through the Free Entry Service,</b> the Participant must first submit a registration. Registration can be done in two ways - via Online Registration and Sweeps Center Free Entry Terminal. 
                 <ul style="font-size:1em; line-height:1; list-style-type:inherit; margin-left:20px;">
                    <br />
                    <li>To register online, Participant must go to www.thesweepscenter.com and fill in the form with his full name, home number and/or mobile number, mailing address and e-mail address. 
                    </li>
                    <br/>
                    <li>To register through the Free Entry Terminal, Participant must visit a Sweeps Center branch and ask the Cashier or Sweeps Assistant for assistance to register on the Free Entry service. Sweeps Assistant shall direct the Participant to the Free Entry Terminal where he shall fill in the same form provided in the online registration.</li>
                 </ul>
              </li>
              <br/>
              <li>The Participant shall be notified instantly if his registration, either through online service or the Sweeps Center Free entry terminal, qualifies for a free entry. After successful registration, the Sweeps Center System shall send a Sweeps code to the Participant's registered e-mail address after the submission of the registration. Successful registration in this case means submitted registration details passed The Sweeps Center's system validations, which include checking of the submitted details and fulfillment of the daily free entry limit.</li>
              <br/>
              <li>Participation via the Free Entry service shall be limited to only one (1) registration per person per day. A "day" is defined as the 24 hours between 12:00:00 am (midnight) and 11:59:59 pm.</li>
              <br/>
              <li>All registration information shall become property of Guam Sweepstakes Corporation.</li>
              <br/>
              <li>Once the Sweeps code has been received by the Participant, he/she must visit any of the Sweeps Center branches so he/she can avail his/her free sweepstakes entry for the day. The Participant must enter his/her Sweeps code by using the Free Entry Terminal inside the Sweeps Center.</li>
              <br/>
              <li>The Participant shall get one (1) free electronic sweepstakes entry for every valid free entry Sweeps code. The Sweeps Code should be existing, unused and unexpired upon entry on the terminals. The Participant must enter this free entry Sweeps code within three days after the code has been sent; otherwise the code will expire and lose validity.</li>
              <br/>
		<li>The Sweeps Center has the right to invalidate free entry codes which are results of Multiple Registrations. </li>
              <br />
              <li><b>To participate through product purchase,</b> the Participant must purchase the Sweeps Center's Internet Browsing Pre-paid card and enter the card PIN on any of the Launch Pad terminals inside the Sweeps Center. The value of his card will be the equivalent number of points which will be loaded to his chosen terminal.</li>
              <br/>
              <li>The loaded points in the terminal shall be the credits used by the Participant to avail of the Internet Browsing service offered by the Sweeps Center. Internet browsing is 10 points per hour. Charging shall be on a per half an hour basis. An hour in this case starts on the current hour and ends at the beginning of the next hour. A half hour is each thirty (30) minute segment that comprises one (1) hour. Five (5) points shall be deducted from the Participant's point balance for every half hour block that he browses the Internet.</li>
              <br/>
              <li>Every purchase of a card guarantees the Participant at least one sweepstakes card at the end of his session. </li>
              <br/>
              <li>The Participant can use up all or part of his points for Internet browsing. However, the number of the e-Sweepstakes entries that the Participant will get at the end of his session will be dependent on what will be left on his points balance. Upon consumption of all his points on Internet browsing, he will be given one (1) e-Sweeps entry. The Participant must convert his zero (0) point balance to be able to get this one (1) e-Sweepstakes entry. If however, the Participant did not use up all of his points on Internet browsing, he must convert his remaining points to e-sweepstakes. Each point shall be converted to one (1) e-Sweepstakes entry.</li>
              <br/>
              <li>The Participant shall have the chance to accumulate more points to be converted into e-Sweepstakes entries by playing the Sweeps Center's games such as horse racing, keno and slots games. Winnings from the games played will be automatically added to his point balance. After game play, the Participant must convert these points to get the equivalent number of sweepstakes entries. One (1) point shall be converted to one (1) e-Sweepstakes entry. If however, the Participant lost all of his points on the games, he must convert his zero (0) point balance to get one (1) e-Sweepstakes entry.</li>
              <br/>
              <li>The Participant with a winning sweepstakes entry shall be notified of the entry's value at the time of participation. All winnings must be redeemed within the day of the participation at the Sweeps Center's cashier. Any winnings unclaimed within the day will be forfeited. Unclaimed prizes will not be awarded.</li>
              <br/>
              <li>Any prize provided by the Sweeps Center for any kind of merchandise or cash is not inclusive of any taxes, registrations, licensing, insurance, postage or delivery charges unless otherwise stipulated by Guam Sweepstakes Corporation. Winner shall be solely responsible for all state, federal and local taxes on the prize. No transfer, refund, substitution or replacement of prizes shall be permitted except that Guam Sweepstakes Corporation reserves the right in its sole discretion to substitute a prize of equal or greater value or its cash equivalent. If winner is disqualified for any reason, the prize will be forfeited.</li>
              <br/>
              <li>The odd of winning from The Sweeps Center is one (1) out of four (4) sweepstakes entries.</li>
              <br/>
              <li>By participating in the sweepstakes, the Participant allows sharing of his personal information with The Sweeps Center, Guam Sweepstakes Corporation and its affiliates and sponsors. The Sweeps Center shall use this information for administration of the sweepstakes service including but not limited to awarding of prizes and future research purposes. By participating in The Sweeps Center and by accepting prizes won, the Participant gives his full consent to Guam Sweepstakes Corporation to use and publish his personal information for any purposes including but not limited to advertising, trade and marketing campaigns of Guam Sweepstakes Corporation and its affiliates and sponsors, in any and all forms of media including but not limited to print, television, radio or world wide web. In addition to this, the Participant allows Guam Sweepstakes Corporation to send to him/her any marketing or promotional material or information about The Sweeps Center and any on-going or upcoming promotions or events.</li>
              <br/>
              <li>The Sweeps Center, Guam Sweepstakes Corporation and its affiliates and sponsors shall not have any obligation or responsibility to award any prizes to Participants with regard to: (1) Participants which gave inaccurate information and/or did not comply with The Sweeps Center's rules and terms and conditions; (2) Participants who have committed fraud in participating in The Sweeps Center's services; (3) prizes that are claimed beyond the redemption period; (4) electronic, hardware, network, software, internet or computer malfunctions, failures and difficulties; (5) any inability of the Participant to accept the prize for any reason; (6) if a prize cannot be awarded due to delays brought about by natural disasters and calamities including but not limited to typhoons, earthquakes and tsunamis or due to acts of terrorism or any other unforeseen events which is beyond The Sweeps Center and Guam Sweepstakes Corporation's control.</li>
              <br/>
              <li>The Sweeps Center and Guam Sweepstakes Corporation have the right to disqualify any Participant or individual who will be caught doing fraudulent activities including but not limited to (1) tampering the Sweep Center's operations; (2) violating these rules and terms and conditions; (3) entering or attempting to enter the Free Sweeps Entry multiple times by using different or fake Participant information on the registration process or through any other devices other than the Free Sweeps Entry terminal located inside The Sweeps Center and the Online Free Entry (www.thesweepscenter.com)</li>
              <br/>
              <li>The participant acknowledges that the random number generator will determine outcome of the Sweeps games and the sweepstakes entries and that the participant accepts the outcomes of all such games. If there is any discrepancy between the results on the Sweeps terminal and the results on the Sweeps games server, the results on the server shall be final and binding.</li>
              <br/>
              <li>If the Sweeps Center computer hardware or the Sweeps games server does not properly implement the rules of a Sweeps game, or does not properly implement the random number generator, or does not properly account for the play that has occurred or if a human error is made by The Sweeps Center or Guam Sweepstakes Corporation, any of its suppliers, or anyone else involved in creating, producing or delivering the Sweeps center, Guam Sweepstakes Corporation will not be liable for any loss including loss of points that results there from. The Participant will forfeit any earned points that result from any error whatsoever.</li>
              <br/>
              <li>If The Sweeps Center and Guam Sweepstakes Corporation determine that technical difficulties or unforeseen events compromise the integrity or viability of the sweepstakes, Guam Sweepstakes Corporation has the right to void, terminate and modify currently used sweepstakes deck.</li>
              <br/>
              <li>The Sweeps Center shall only award the stated number of prizes. If any error causes more than the stated number of prizes to be awarded or claimed, Guam Sweepstakes Corporation reserves the right to award only the previously stated number of prizes.</li>
              <br />
              <li>By participating in the services offered by The Sweeps Center, Participants and winners indemnify and hold harmless Guam Sweepstakes Corporation, its software providers and their parent companies, affiliates, subsidiaries, associates and all of their officers, employees, directors, from any suit, demand, cause of action, claim liability, costs, expenses (including reasonable legal fees), damages, fines, penalties, fees or any other charges whatsoever that may arise during the participation in the sweepstakes, possession and misuse of prizes or as a result of the Participant's breach of these terms and conditions or the rules of any of the Sweeps games in the Sweeps Center.</li>
              <br />
              <li>Title, ownership and all intellectual property rights in the software used in The Sweeps Center remain with the original licensor's, Guam Sweepstakes Corporation's software providers (and to an extent, particularly in relation to the intellectual property rights associated with The Sweeps Center name, with Guam Sweepstakes Corporation and its associated companies), including images, photographs, animations, video, audio, music and text that may be part of the software. The content of the software used in the sweepstakes and the Sweeps games is protected as a collected work under applicable copyright laws. The copying, redistribution or publication of any of the content or part of the software is strictly prohibited. Title, ownership and all intellectual property rights in the name "The Sweeps Center" vest in Guam Sweepstakes Corporation. The Participant or any individual shall not attempt to tamper and/or modify the software provided by The Sweeps Center or the software connected to sweepstakes and Sweeps games.</li>
              <br />
              <li>The sweepstakes and Sweeps games software is provided to the Participant in an "as is" basis, without representation or warranty of any kind. Guam Sweepstakes Corporation excludes all implied terms, conditions and warranties (including any warranty or merchantability, satisfactory quality and fitness for any particular purpose) and does not warrant that the software will meet the Participant's requirements.</li>
              <br />
              <li>Neither Guam Sweepstakes Corporation nor its software provider, nor any of their affiliates and related parties warrant or guarantee that the software will be non-infringing, that the operation of the software will be error-free or uninterrupted, that any defects in the software will be corrected, that the software or the servers are free of viruses and bugs, or the privacy, security, authenticity and non-corruption of any information transmitted through, or stored in any system connected to the internet. In case there is an unexpected system failure encountered, the system is "smart enough" to select the electronic sweepstakes cards for the Participant.</li>
              <br />
              <li>Guam Sweepstakes Corporation may amend these terms and conditions or the rules of any of the sweeps services from time to time. It is the sole responsibility of the Participant to learn of any such updates and changes. The Participant's participation on the Sweeps Center following any such change shall constitute the Participant's agreement to the change whether or not the Participant has actual notice of, or has read the relevant changes. If the Participant does not agree to be bound by relevant changes, he should not continue to participate in the services of the Sweeps Center any further.</li>
              <br />
              <li>The original text of this agreement is in English and any interpretation of this agreement will be based on the original English text. If this agreement or any documents or notices related to it are translated into any other language, the original English version will prevail.</li>
              <br />    
           </ul>
        </div>
            <br/><br/>
            <div align="center"><label><input type="checkbox" id="terms" name="terms" />I have read and agree with Sweeps Center&apos;s Terms and Conditions.</label></div>
            <br/>
            <img src="images/ProceedButton.png" alt="" style="cursor: pointer;" onclick="checkform();" />
        </div>
	<br/><br/><br/>
    </div>
</html>

<script>
function isChecked()
{
    document.getElementById("terms").checked = true;
}
</script>

