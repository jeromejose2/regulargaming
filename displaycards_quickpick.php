<?php
ini_set('display_errors', 1);
ini_set('log_errors', 1);
session_start();
include('controller/trans.php');
include('controller/cdisplaycards_quickpick.php');
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/main.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
<link href="css/lightbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="js/CSSPopUp3.js"></script>
<script language="javascript" type="text/javascript" src="js/trans.js"></script>
<script language="javascript" type="text/javascript" src="js/disable_f5.js"></script>
<script language="javascript" type="text/javascript" src="js/disable_rightclick.js"></script>
<script language="javascript" type="text/javascript" src="js/quick_pick_button.js"></script>
<script src="js/jquery-1.4.1.js" type="text/javascript"></script>
<script src="js/jquery.background.image.scale-0.1.js" type="text/javascript"></script>
<script type="text/javascript">
        //Using document.ready causes issues with Safari when the page loads
        jQuery(window).load(function(){
                $("#contentContainer").backgroundScale({
                        imageSelector: "#gaBG",
                        centerAlign: true,
                        containerPadding: 0
                });
        });
</script>
<script type="text/javascript">
function heartbeat()
{
    xajax_HeartBeat();
}

setInterval ("heartbeat()", 600000);
//setInterval ("heartbeat()", 100000);
</script>
<title>eSweeps</title>
<?php $xajax->printJavascript(); ?>
</head>

<body>
<div id="blanket" style="display:none;"></div>
<div id="popUpDivOpenedCards" style="display:none; border:solid; border-color:grey; font-family:Helvetica; font-size: 20px;">
<!--    <div align="center" style=" border-bottom-style: solid; border-color:grey; background-color: black; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 600px;">Opened Cards</div></b></div>-->
    <div id="popup_container_displaycards">
        <div id="opened_cards"></div>
    </div>
    <div>
        <div class="quick8" >
          <div class="sweeps_quick8"  ></div>
          <div class="sweeps_back8" onclick="popup('popUpDivOpenedCards');"></div>
        </div>
    </div>
</div>

<div id="light" class="white_content"><?php include('mechanics.php') ?></div>
<div id="light2" class="white_content"><?php include('terms.php') ?></div>

  <div id="mainContainer">
    	<div id="banner">
        	<div id="logo_landing2"> <img src="images/theSweepsLogo.png" alt="" height="150" width="250" /></div>
    	</div>
        <div id="contentContainer" style="height: 75%; top: 170px;">
            <img id="gaBG" src="images/contentbg2.jpg" height="577px" alt="" />
            <div id="sweepsMainContainer">
            	<?php echo $html_content ?>
            </div>
            <label class="winsumm123"><?php echo $resultmsg ?></label>
            <div class="quick123" >
                <div><?php echo $okbtn ?></div>
            </div>
        </div>
        <div id="footer">
        	<div id="footerBox">
            	<div class="footerBox_left"></div>
              	<div class="footerBox_body">
                    <div class="under18"></div>
                    <div class="rules" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Rules &amp; Mechanics</div>
                    <div class="terms" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'">Terms &amp; Conditions</div>
              	</div>
                <div class="footerBox_right"></div>
            </div>
         </div>
    </div>
</body>
</html>
