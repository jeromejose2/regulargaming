<?php
ini_set('display_errors', 1);
ini_set('log_errors', 1);
session_start();
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login : Launch Pad</title>
<link rel="stylesheet" type="text/css" href="css/login.css" />
<link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
<link href="css/lightbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="js/CSSPopUp.js"></script>
<script language="javascript" type="text/javascript" src="js/form_validation.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script language="javascript" type="text/javascript" src="js/lightbox.js"></script>
<script language="javascript">
$(document).ready(function(){
    $('#btnLogin').click(function(){
        if(checkform_reg(document.getElementById('frmLogin'))) {
            get_mac_address(function(){
                //alert('pause');
                //$('#frmLogin').submit();
            });
        } else{
            //alert('test');
        }
    });

})


function get_mac_address(onSuccess)
{
    show_loading();
    macs.getMacAddress();
    var macadd = document.macaddressapplet.getMacAddress();
    var usr = document.getElementById("textfield2").value;
    var pwd = document.getElementById("txtpassword").value;

    $.ajax({
         url: 'controller/register_terminalajax.php?macadd='+macadd+'&usr='+usr+'&pwd='+pwd,
         type : 'post',
         success : function(data)
         {
            var json = jQuery.parseJSON(data);
            hide_loading();
            $("#err_msg").html(json.msg1);
            //$("#btnLogin").html(json.msg2);
            if(json.msg1 == "Username/Password is invalid. Please try again.")
            {

            }
            else
            {
                $('#btnLogin').removeClass('login2').addClass('proceed');
                $('#btnLogin').attr('onclick', 'window.location.href="index.php"');
            }
            onSuccess();
         },
         error: function(e)
         {
            //alert(e.responseText);
         }
      });
}

function checkform_reg ( form )
{
    if (form.txtuser.value == "")
    {
        popup('popUpDivLoginUname');
        form.txtuser.focus();
        return false ;
    }

    else
    {
        return true;
    }
}

</script>
<style>
    .txtName2{
	font: bold 20px/25px "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	color: #FFF;
	text-transform: uppercase;
	width:150px;
	height:auto;
	margin:10px 0 0 15px;
	float:left;
        width: 100%;
        text-align: center;
    }
    .login2 {
	background:url(images/registerred.png);
	width:210px;
	height:55px;
        cursor: pointer;
        margin-top: 10px;
    }
    .proceed {
	background:url(images/ProceedButton.png);
	width:146px;
	height:47px;
        cursor: pointer;
        margin-top: 10px;
    }
</style>
</head>

<body>
<div id="light3" class="white_content2"><div align="center"><br/><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 30px;" /></div></div>
<div id="fade" class="black_overlay"></div>

<div id="mainContainer">
<div id="blanket" style="display:none;"></div>
<div id="popUpDivLoginUname" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#139E9E; background-color: #77A6A0; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 200px;">INVALID LOG IN</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="convert" align="center"></div><div id="convert_img" align="center">Please enter a username.</div>
        <div id="okbtn" align="center" style="margin-top: 20px;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivLoginUname');" style="cursor:pointer;"/></div>
    </div>
</div>

    <div id="banner"></div>
    <div id="contentContainer">
        <div id="Login_Cont">
            <div class="login_logo"></div>
            <div class="login_body">
                <form id="frmLogin" method="post" action="">
                <div class="loginCont_body">
                    <div class="txtName">USERNAME</div>
                    <div class="inputbox">
                         <input type="text" name="txtuser" id="textfield2" class="inputBoxEffect" />
                     </div>
                    <div class="txtName">PASSWORD</div>
                     <div class="inputbox">
                         <input type="password" name="txtpass" id="txtpassword" class="inputBoxEffect" />
                     </div>
                     <div class="loginCont_btn" style="width: 100%; text-align: center;">
                         <input type="button" value="" id="btnLogin" class="login2" />
                    </div>
                </div>
                </form>
                <div class="login_bottom">&nbsp;</div>
                <div id="err_msg" align="center"></div>
            </div>
        </div>
        <div id="footer"></div>
    </div>
</div>

<!--[if !IE]> Firefox and others will use outer object -->
<embed type="application/x-java-applet"
       name="macaddressapplet"
       width="0"
       height="0"
       code="MacAddressApplet.class"
       archive="SMacAddressApplet.jar"
       pluginspage="http://java.sun.com/javase/downloads/index.jsp"
       style="position:absolute; top:-1000px; left:-1000px;">
    <noembed>
    <!--<![endif]-->
        <!---->
        <object classid="clsid:CAFEEFAC-0016-0000-FFFF-ABCDEFFEDCBA"
                type="application/x-java-applet"
                name="macaddressapplet"
                style="position:absolute; top:-1000px; left:-1000px;"
                >
            <param name="code" value="MacAddressApplet.class">
            <param name="archive" value="SMacAddressApplet.jar" >
            <param name="mayscript" value="true">
            <param name="scriptable" value="true">
            <param name="width" value="0">
            <param name="height" value="0">
          </object>
    <!--[if !IE]> Firefox and others will use outer object -->
    </noembed>
</embed>
<!--<![endif]-->

<script type="text/javascript">
var macs = {
    getMacAddress : function()
    {
        document.macaddressapplet.setSep( "-" );
        //alert( "Mac Address = " + document.macaddressapplet.getMacAddress() );
    },
    getCPUProduct : function() {
        alert( "CPU Product = " + document.macaddressapplet.getProduct() );
    },
    getCPUVendor : function() {
        alert("CPU Vendor"+ document.macaddressapplet.getVendor());
    },
    getCPUVersion : function() {
        alert("CPU Version"+ document.macaddressapplet.getVersion());
    },
    getCPUSerial : function() {
        alert("CPU Serial"+ document.macaddressapplet.getSerial());
    },
    getMacAddressesJSON : function()
    {
        document.macaddressapplet.setSep( ":" );
        document.macaddressapplet.setFormat( "%02x" );
        var macs = eval( String( document.macaddressapplet.getMacAddressesJSON() ) );
        var mac_string = "";
        for( var idx = 0; idx < macs.length; idx ++ )
            mac_string += "\t" + macs[ idx ] + "\n ";

        alert( "Mac Addresses = \n" + mac_string );
    }
}
</script>

</body>
</html>
