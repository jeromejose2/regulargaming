<?php
ini_set('display_errors', 1);
ini_set('log_errors', 1);
session_start();
include('controller/trans.php');
include('controller/clobby.php');
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="css/launchpad.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
<link href="css/lightbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="js/trans.js"></script>
<script language="javascript" type="text/javascript" src="js/CSSPopUp.js"></script>
<script language="javascript" type="text/javascript" src="js/convert.js"></script>
<script src="js/jquery-1.4.1.js" type="text/javascript"></script>
<script src="js/jquery.background.image.scale-0.1.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="js/lightbox.js"></script>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/easySlider1.5.js"></script>
<script type="text/javascript" src="js/easySlider1.5_1.js"></script>

<?php $xajax->printJavascript(); ?>

<script type="text/javascript">
	var next = 'NEXT1';
        var prev = 'PREVIOUS1';
        $(document).ready(function(){	
		$("#slider").easySlider({
			prevText: prev,
			nextText: next,		
			lastShow: false,
			vertical: true
//                        continuous: true
                      
		});
                $("#slider1").Slider({
                	
			lastShow: false,
			vertical: true
//                         continuous: true
		});
              
              
	});	
</script>
</head>



<style>
    body {
	margin:0;
	padding:40px;
	background:#fff;
	font:80% Arial, Helvetica, sans-serif;
	color:#555;
	line-height:180%;
}
h1{
	font-size:180%;
	font-weight:normal;
	}
h2{
	font-size:160%;
	font-weight:normal;
	}	
h3{
	font-size:140%;
	font-weight:normal;
	}	
img{border:none;}
pre{
	display:block;
	font:12px "Courier New", Courier, monospace;
	padding:10px;
	border:1px solid #bae2f0;
	background:#e3f4f9;	
	margin:.5em 0;
	width:500px;
	}		

/* Easy Slider */

	#slider ul, #slider li{
		margin:0;
		padding:0;
		list-style:none;
		}
	#slider li{ 
		/* 
			define width and height of list item (slide)
			entire slider area will adjust according to the parameters provided here
		*/ 
		width:800px;
		height:300px;
		overflow:hidden; 
		}
                span#prevBtn{
                    
                }
                span#nextBtn{
                 
                }					

/* // Easy Slider */
#slider1 ul, #slider1 li{
		margin:0;
		padding:0;
		list-style:none;
		}
	#slider1 li{ 
		/* 
			define width and height of list item (slide)
			entire slider area will adjust according to the parameters provided here
		*/ 
		width:800px;
		height:200px;
		overflow:hidden; 
		}

</style>

<body>
 
	<div id="slider1">
		<ul>				
			<li>
                            <div style="margin-left:-5px;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/kenowithBG.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>KENO</p></div>
                            </div>
                             <div style="margin-left:200px;margin-top:-170px;" class="horseRacing" onclick="check_session('qhorse1');">
					<img src="images/horseracingwithBG1.png" height="150px" width="180px"/>
					<div class="gameTitles">QUARTER HORSE RACING</div>
                            </div>
                            <div style="margin-left:400px;margin-top:-188px;" class="horseRacing" onclick="check_session('dino');">
					<img src="images/dino.png" height="150px" width="180px"/>
					<div class="gameTitles">DINO MIGHT</div>
                            </div>
                            <div style="margin-left:600px;margin-top:-171px;" class="horseRacing" onclick="check_session('tomb');">
					<img src="images/TombRaider.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>TOMB RAIDER</p></div>
                            </div>
                         </li>
                         <li>
                            <div style="margin-left:0px;" class="horseRacing" onclick="check_session('bj');">
					<img src="images/BJBG.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>BLACK JACK</p></div>
                            </div>
                             <div style="margin-left:200px;margin-top:-170px;" class="horseRacing" onclick="check_session('tomb2');">
					<img src="images/TombRaider2BG.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>TOMB RAIDER: SECRET OF THE SWORD</p></div>
                            </div>
                            <div style="margin-left:400px;margin-top:-188px;" class="horseRacing" onclick="check_session('wheel');">
					<img src="images/WheelofWealth1.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>WHEEL OF WEALTH</p></div>
                            </div>
                            <div style="margin-left:600px;margin-top:-171px;" class="horseRacing" onclick="check_session('bigkahuna');">
					<img src="images/BigKahuna.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>BIG KAHUNA</p></div>
                            </div>
                         </li>
<!--                         <li>
                            <div style="margin-left:0px;" class="horseRacing" onclick="check_session('goldfactory');">
					<img src="images/GoldFactory.png" height="150px" width="180px" />
					<div class="gameTitles"><p>GOLD FACTORY</p></div>
                            </div>
                             <div style="margin-left:200px;margin-top:-195px;" class="horseRacing" onclick="check_session('karatepig');">
					<img src="images/KaratePig.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>KARATE PIG</p></div>
                            </div>
                            <div style="margin-left:400px;margin-top:-192px;" class="horseRacing" onclick="check_session('surewin');">
					<img src="images/SureWin.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>SURE WIN</p></div>
                            </div>
                            <div style="margin-left:600px;margin-top:-193px;" class="horseRacing" onclick="check_session('thunderstruck');">
					<img src="images/ThunderStruckII.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>THUNDER STRUCK II</p></div>
                            </div>
                         </li>-->

                  
               </ul>
		
            
          
	</div>
    
   
	<div id="slider">
		<ul>				
			
                    <li>
                              <div style="margin-left:0px;margin-top:0px;" class="horseRacing" onclick="check_session('bj');">
					<img src="images/BJBG.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>BLACK JACK</p></div>
                            </div>
                         <div style="margin-left:200px;margin-top:-170px;" class="horseRacing" onclick="check_session('tomb2');">
						<img src="images/TombRaider2BG.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>TOMB RAIDER: SECRET OF THE SWORD</p></div>
                            </div>
                          <div style="margin-left:400px;margin-top:-187px;" class="horseRacing" onclick="check_session('wheel');">
					<img src="images/WheelofWealth1.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>WHEEL OF WEALTH</p></div>
                            </div>
                            <div style="margin-left:600px;margin-top:-175px;" class="horseRacing" onclick="check_session('bigkahuna');">
					<img src="images/BigKahuna.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>BIG KAHUNA</p></div>
                            </div>
                
			</li>
                    
                  <li>
                              <div style="margin-left:0px;margin-top:0px;" class="horseRacing" onclick="check_session('goldfactory');">
					<img src="images/GoldFactory.png" height="150px" width="180px" />
					<div class="gameTitles"><p>GOLD FACTORY</p></div>
                            </div>
                         <div style="margin-left:200px;margin-top:-170px;" class="horseRacing" onclick="check_session('karatepig');">
				<img src="images/KaratePig.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>KARATE PIG</p></div>
                            </div>
                          <div style="margin-left:400px;margin-top:-175px;" class="horseRacing" onclick="check_session('surewin');">
					<img src="images/SureWin.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>SURE WIN</p></div>
                            </div>
                            <div style="margin-left:600px;margin-top:-170px;" class="horseRacing" onclick="check_session('thunderstruck');">
						<img src="images/ThunderStruckII.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>THUNDER STRUCK II</p></div>
                            </div>
                
			</li> 
<!--                        <li>
                             
                
			</li>-->
                   
               </ul>
		
            
          
	</div>
    
    
    
    
    
  
    
    
    
    
            


</body>
</html>