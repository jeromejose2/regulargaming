<?php
#Name: RealtimeGamingCashierAPI.class.php
#Author: FTG
#Version: 1.0.0
#Copyright 2010 PhilWeb Corporation

require_once('nusoap/nusoap.php');
//$_RealtimeGamingCashierAPI = new RealtimeGamingCashierAPI($wsdlUrl, $certfile, $keyfile, $passphrase);

class RealtimeGamingCashierAPI
{
    private $_soapClient;

    public function __construct()
    {
        $this->_soapClient = new nusoap_client('https://58.145.199.51/FRNDSNLNCSNRRGTAMJGS/processor/processorapi/cashier.asmx?wsdl', 'wsdl');
        $this->_soapClient->authtype = 'certificate';
        $this->_soapClient->certRequest['sslcertfile'] = 'app_data/RTGClientCerts/cert.pem';
        $this->_soapClient->certRequest['sslkeyfile'] = 'app_data/RTGClientCerts/key.pem';
        $this->_soapClient->certRequest['passphrase'] = '';
        $this->_soapClient->certRequest['verifypeer'] = 0;
        $this->_soapClient->certRequest['verifyhost'] = 0;
    }

    public function GetError()
    {
            return $this->_soapClient->getError();
    }

    public function DepositGeneric($casinoID = 1, $PID, $methodID = 503, $amount, $tracking1, $tracking2, $tracking3, $tracking4, $sessionID, $userID = 0, $skinID = 1)
    {
        $result = $this->_soapClient->call('DepositGeneric', array('casinoID' => $casinoID,
                                                                   'PID' => $PID,
                                                                   'methodID' => $methodID,
																   'amount' => $amount,
                                                                   'tracking1' => $tracking1,
                                                                   'tracking2' => $tracking2,
                                                                   'tracking3' => $tracking3,
                                                                   'tracking4' => $tracking4,
                                                                   'sessionID' => $sessionID,
                                                                   'userID' => $userID,
                                                                   'SkinID' => $skinID));

        return $result;
    }

    public function GetAccountBalance($casinoID = 1, $PID, $forMoney = 1)
    {
        $result = $this->_soapClient->call('GetAccountBalance', array('casinoID' => $casinoID,
                                                                      'PID' => $PID,
                                                                      'forMoney' => $forMoney));

        return $result;
    }

    public function GetAccountInfoByPID($casinoID = 1, $PID)
    {
        $result = $this->_soapClient->call('GetAccountInfoByPID', array('casinoID' => $casinoID,
                                                                        'PID' => $PID));

        return $result;
    }

    public function GetPIDFromLogin($login)
    {
        $result = $this->_soapClient->call('GetPIDFromLogin', array('login' => $login));

        return $result;
    }

    public function Login($casinoID = 1, $PID, $hashedPassword, $forMoney = 1, $IP, $skinID = 1)
    {
        $result = $this->_soapClient->call('Login', array('casinoID' => $casinoID,
                                                          'PID' => $PID,
                                                          'hashedPassword' => $hashedPassword,
                                                          'forMoney' => $forMoney,
                                                          'IP' => $IP,
                                                          'skinID' => $skinID));

        return $result;
    }

    public function WithdrawGeneric($casinoID = 1, $PID, $methodID = 502, $amount, $tracking1, $tracking2, $tracking3, $tracking4, $sessionID, $userID = 0, $skinID = 1)
    {
        $result = $this->_soapClient->call('WithdrawGeneric', array('casinoID' => $casinoID,
                                                                    'PID' => $PID,
                                                                    'methodID' => $methodID,
                                                                    'amount' => $amount,
                                                                    'tracking1' => $tracking1,
                                                                    'tracking2' => $tracking2,
                                                                    'tracking3' => $tracking3,
                                                                    'tracking4' => $tracking4,
                                                                    'sessionID' => $sessionID,
                                                                    'userID' => $userID,
                                                                    'skinID' => $skinID));

        return $result;
    }

    public function TrackingInfoTransactionSearch($PID, $tracking1, $tracking2 = '', $tracking3 = '', $tracking4 = '')
    {
        $result = $this->_soapClient->call('TrackingInfoTransactionSearch', array('pid' => $PID,
                                                                                  'tracking1' => $tracking1,
                                                                                  'tracking2' => $tracking2,
                                                                                  'tracking3' => $tracking3,
                                                                                  'tracking4' => $tracking4));

        return $result;
    }

}

?>