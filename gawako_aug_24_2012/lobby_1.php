<?php
ini_set('display_errors', 1);
ini_set('log_errors', 1);
session_start();
include('controller/trans.php');
include('controller/clobby.php');
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="css/launchpad.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
<link href="css/lightbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="js/trans.js"></script>
<script language="javascript" type="text/javascript" src="js/CSSPopUp.js"></script>
<script language="javascript" type="text/javascript" src="js/convert.js"></script>
<script src="js/jquery-1.4.1.js" type="text/javascript"></script>
<script src="js/jquery.background.image.scale-0.1.js" type="text/javascript"></script>
<script src="js/ypSimpleScrollC.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="js/lightbox.js"></script>

<script type="text/javascript">
       var test  = new ypSimpleScroll("myScroll", -1060, 50, 900, 250, 40, 500)
       //Using document.ready causes issues with Safari when the page loads
        jQuery(window).load(function(){
                $("#contentContainer").backgroundScale({
                        imageSelector: "#gaBG",
                        centerAlign: true,
                        containerPadding: 0
                });
        });
</script>
<title>Games List</title>
<?php $xajax->printJavascript(); ?>
</head>

<body onload="do_getbalance();test.load();">

<div id="blanket" style="display:none;"></div>
<div id="popUpDivConvert" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 180px;">CONVERT POINTS</div></b></div>
    <div id="popup_container_home">
        <div id="convert" align="center"></div><div id="convert_img" align="center" style="visibility:hidden;"><img src="images/load_bal.gif" height="20px" /></div>
        <div id="okbtn" align="center" style="margin-top: 0px;"></div><div id="okbtn_img" align="center" style="margin-top: -20px; visibility:hidden;">PROCESSING</div>
    </div>
</div>

<div id="popUpDivLPConvert" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 100px;">CONVERT POINTS CONFIRMATION</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="convert" align="center"></div><div id="convert_img" align="center"><p>Are you sure you want to convert your <label id="convbal"></label></p><br/> <p> point/s and end your gaming session now?</p></div>
        <div id="okbtn" align="center" style="margin-top: 40px; margin-left: 80px; float: left;"><img src="images/OK Button.png" alt="" onclick="convert_points(); popup('popUpDivLPConvert');" style="cursor:pointer;"/></div><div style="margin-top: 35px;"><img src="images/cancelbutton.png" alt="" onclick="popup('popUpDivLPConvert');" style="cursor:pointer;"/></div>
    </div>
</div>

<div id="popUpDivLPCheckActiveSession" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 230px;">ALERT</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="msg" align="center"></div>
        <div id="okbtn" align="center" style="margin-top: 20px;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivLPCheckActiveSession');" style="cursor:pointer;"/></div>
    </div>
</div>

<div id="light" class="white_content"><?php include('mechanics.php') ?></div>
<div id="light2" class="white_content"><?php include('terms.php') ?></div>
<div id="light3" class="white_content2"><div align="center"><br/><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 30px;" /></div></div>
<div id="fade" class="black_overlay"></div>

    <div id="mainContainer">
    	<div id="banner">
            <table width="100%" border="0">
                <tr>
                    <td align="center">
                        <!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
                        <img src="images/theSweepsLogo.png" alt="" height="190" width="270" /></td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td align="center">
                        <div style="color: white;">Logged In As:
                            <?php
                            $login = $_SESSION['user'];
                            $new_string = ereg_replace("[^0-9]", "", $login);

                            echo "Terminal ".$new_string;
                            ?>
                        </div>
		    	<div id="txtBoxContainer_point">
                            <div class="txtBox_left"></div>
                            <div class="txtBox_body"><img src="images/load_bal.gif" id="load_bal_img" alt="" style="margin-left: 20px; margin-top: 10px; width: 180px;" /><div id="balance"></div></div>
		            <div class="txtBox_right"></div>
		        </div>
		        <div id="btnContainer">
		        	<div class="convertPoints" onclick="show_loading(); xajax_GetBalanceConv();">
                                    <img src="images/convertPoints.png" height="33px" width="150px" />
                                </div>
                                <?php echo $conv_btn ?>
                                <div class="enterCode" onclick="xajax_CheckSessionSweepsCode();">
                                    <img src="images/enterCode.png" height="33px" width="160px" />
                                </div>
		        </div>
                    </td>
                    <td align="center">
                        <div id="adContainer">
                            <a href="">
                            <img src="images/5000.gif" height="155px" width="245px" style="margin-top: 30px; margin-right: 2px;"/></a>
                            <div class="adContent">
                                <img src="images/adContainer.png" height="170px" width="265px" style="margin-top: -160px; margin-right: 12px;"/>
                            </div>
                        </div>
                    </td>
                </tr>

            </table>
        </div>

            <div id="contentContainer">
            <img id="gaBG" src="images/contentbg.jpg" />
			<div style="float:left;width:150px;margin-left: 780px;height:50px;margin-top:20px">
				<div class="back_launchPad" onclick="location.href='launchpad.php';"></div>		
			</div>
<div id="myScrollContainer">
<div  id="myScrollContent">
                          
        
        
         <div style="margin-top:10%;margin-left:150px;margin-left:120px;width:1100px;">
				<div style="margin-top:50px;margin-left:-84%;" class="horseRacing" onclick="check_session('bj');">
                                        <img src="images/kenowithBG.png" height="150px" width="180px"/>&nbsp;&nbsp; &nbsp; &nbsp;
                                   	<div class="gameTitles"><p>KENO</p></div>
				</div>
				<div style="margin-top:-175px;margin-left:-50%;" class="secretsword" onclick="check_session('tomb2');">
					<img src="images/horseracingwithBG1.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>QUARTER HORSE RACING</p></div>
				</div>
				<div style="margin-top:-170px;margin-left:-15%;" class="wheelwealth" onclick="check_session('wheel');">
					<img src="images/dino.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>DINO MIGHT</p></div>
				</div>
                                <div style="margin-top:-172px;margin-left:20%;" class="wheelwealth" onclick="check_session('wheel');">
                                                    <img src="images/TombRaider.png" height="150px" width="180px"/>
                                                    <div class="gameTitles"><p>TOMB RAIDER</p></div>
                                            </div>
                    
			</div>
                
           	<div style="margin-top:2%;margin-left:150px;margin-left:120px;width:1100px;">
				<div style="margin-top:50px;margin-left:-84%;" class="horseRacing" onclick="check_session('bj');">
                                    <img src="images/BJBG.png" height="150px" width="180px"/>&nbsp;&nbsp; &nbsp; &nbsp;
                                    <div class="gameTitles"><p>BLACK JACK</p></div>
				</div>
				<div style="margin-top:-175px;margin-left:-50%;" class="secretsword" onclick="check_session('tomb2');">
                                    <img src="images/TombRaider2BG.png" height="150px" width="180px"/>
                                    <div class="gameTitles"><p>TOMB RAIDER:<br>SECRET OF THE SWORD</p></div>
				</div>
				<div style="margin-top:-188px;margin-left:-15%;" class="wheelwealth" onclick="check_session('wheel');">
                                    <img src="images/WheelofWealth1.png" height="150px" width="180px"/>
                                    <div class="gameTitles"><p>WHEEL OF WEALTH</p></div>
				</div>
                                <div style="margin-top:-172px;margin-left:20%;" class="wheelwealth" onclick="check_session('wheel');">
                                    <img src="images/WheelofWealth1.png" height="150px" width="180px"/>
                                    <div class="gameTitles"><p>WHEEL OF WEALTH</p></div>
                                </div>
                    
			</div>
    
    
                <div style="margin-top:10%;margin-left:150px;margin-left:120px;padding-bottom: 20%;width:1100px;">
				<div style="margin-top:50px;margin-left:-84%;" class="horseRacing" onclick="check_session('bj');">
                                    <img src="images/BJBG.png" height="150px" width="180px"/>&nbsp;&nbsp; &nbsp; &nbsp;
                                    <div class="gameTitles"><p>BLACK JACK</p></div>
				</div>
				<div style="margin-top:-175px;margin-left:-50%;" class="secretsword" onclick="check_session('tomb2');">
                                    <img src="images/TombRaider2BG.png" height="150px" width="180px"/>
                                    <div class="gameTitles"><p>TOMB RAIDER:<br>SECRET OF THE SWORD</p></div>
				</div>
				<div style="margin-top:-188px;margin-left:-15%;" class="wheelwealth" onclick="check_session('wheel');">
                                    <img src="images/WheelofWealth1.png" height="150px" width="180px"/>
                                    <div class="gameTitles"><p>WHEEL OF WEALTH</p></div>
				</div>
                                <div style="margin-top:-172px;margin-left:20%;" class="wheelwealth" onclick="check_session('wheel');">
                                    <img src="images/WheelofWealth1.png" height="150px" width="180px"/>
                                    <div class="gameTitles"><p>WHEEL OF WEALTH</p></div>
                                </div>
                    
			</div>
    
            
            
</div>   
</div>
            
                <div id="msg" style="float: right; margin-top: -45px; margin-right: 65%;">
                    <a href="#" onmouseover="test.scrollNorth()" onmouseout="test.endScroll()">
                        <div style="color:#BDB76B;">
                            <img src="images/scroll_up.png" alt="^"border="2" style="border-color:#BDB76B;" />
                        </div>
                    </a>
                        <br/>

                    <a href="#" onmouseover="test.scrollSouth()" onmouseout="test.endScroll()">
                        <div style="color:#BDB76B; margin-top:5px">
                            <img src="images/scroll_down.png" alt="v"/>
                        </div>
                    </a>
            </div>
            
        </div>
        
        
        
        
        
        
        
        
    
<!--                <div id="myScrollContainer">
                <div  id="myScrollContent">-->
    
                    
                    
<!--                </div>
                </div>
            -->
            
        

            
        <div id="footer">
            <div id="footerBox">
                <div class="footerBox_left"></div>
                <div class="footerBox_body">
                    <div class="under18"></div>
                    <div class="rules" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Rules &amp; Mechanics</div>
                    <div class="terms" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'">Terms &amp; Conditions</div>
                </div>
                <div class="footerBox_right"></div>
            </div>
         </div>

    </div>
</body>
</html>
