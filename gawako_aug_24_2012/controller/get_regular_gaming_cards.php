<?php
ini_set('display_errors', 1);
ini_set('log_errors', 1);

session_start();


/*start add by mtcc 06292012 */
if (!isset($_SESSION['equivalentpoints']) ||!isset($_SESSION['convertedbalance']))
{
    header("Location: index.php");
}

/*end add by mtcc 06292012 */

$login = $_SESSION['user'];
$id = $_SESSION['id'];
$siteid = $_SESSION['siteid'];
$ip = $_SESSION['ip'];
$convertedpts = $_SESSION['equivalentpoints'];
$convertedbalance = $_SESSION['convertedbalance']; //added by mtcc on 11/02/2011
if(isset ($_SESSION['sweeps_code']))
{
    $sweeps_code = $_SESSION['sweeps_code'];
}
else
{
    $sweeps_code = '';
}


include('../../conn/connstr.php');


if(isset ($_SESSION['IsFreeEntry']))
{
    $swps_cde = $_SESSION['sweeps_code'];

    //$result = mysqli_query($dbConn, "CALL proc_getfreeentry('$id','$ip','$sweeps_code');");
    $result= mysqli_query($dbConn, "CALL chancecards.proc_getprize(2,'1',0.10,'$id',2,'$swps_cde');");

    //insert to logs
    $filename = "../../DBLogs/logs.txt";
    $fp = fopen($filename , "a");

    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || ".$login." || proc_getprize(2,'1',0.10,'$id',2,'$swps_cde') \r\n");

    fclose($fp);
}
else
{
    //$result= mysqli_query($dbConn, "CALL chancecards.proc_getprize(2,'$convertedpts','$id',1,'');");
    $result= mysqli_query($dbConn, "CALL chancecards.proc_getprize(2,'$convertedpts','$convertedbalance','$id',1,'');");

    //insert to logs
    $filename = "../../DBLogs/logs.txt";
    $fp = fopen($filename , "a");

    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || ".$login." || proc_getprize(2,'$convertedpts','$convertedbalance','$id',1,'') \r\n");

    fclose($fp);
}

/*start add by mtcc 06292012 */
unset($_SESSION['equivalentpoints']);
unset($_SESSION['convertedbalance']);
/*end add by mtcc 06292012 */

$count = 0;


while ($array= mysqli_fetch_array($result))
{
    if($array['PrizeDescription'] == '$0 Cash')
    {
        $array['PrizeDescription'] = 'Try Again';
        $wintype[$count] = $array['PrizeDescription'];

        $ecn[$count] = $array['ECN'];
        $resultmsg[$count] = $array['ResultMsg'];

        $_SESSION['$wintype['.$count.']'] = $wintype[$count];
        $_SESSION['$ecn['.$count.']'] = $ecn[$count];
        $_SESSION['$resultmsg['.$count.']'] = $resultmsg[$count];
    }
    else
    {
        $wintype[$count] = $array['PrizeDescription'];

        $ecn[$count] = $array['ECN'];
        $resultmsg[$count] = $array['ResultMsg'];

        $_SESSION['$wintype['.$count.']'] = $wintype[$count];
        $_SESSION['$ecn['.$count.']'] = $ecn[$count];
        $_SESSION['$resultmsg['.$count.']'] = $resultmsg[$count];
    }
	$servicetransid = $array['TransactionSummaryID'];
    $count++;
}
mysqli_next_result($dbConn);
mysqli_free_result($result);

//Added By Arlene R. Salazar 05-30-2012
/*$query4 = "SELECT ID FROM cafino.tbl_terminalsessions where terminalid = '".$_SESSION['id']."' and dateend = 0";
$result4= mysqli_query($dbConn, $query4);
while($row2= mysqli_fetch_array($result4))
{
	$terminalsessionid = $row2[0];
}
mysqli_next_result($dbConn);
mysqli_free_result($result4);*/
//$terminalsessionid = $_SESSION['tsi'];
/*start add by mtcc 06292012 */
if (isset($_SESSION['tsi']))
{
    $terminalsessionid = $_SESSION['tsi'];
}
else
{
    $query2 = "SELECT ID FROM cafino.tbl_terminalsessions where terminalid = '".$id."' and dateend = 0";
    $result2= mysqli_query($dbConn, $query2);
    while($row2= mysqli_fetch_array($result2))
    {
            $terminalsessionid = $row2[0];
    }
    mysqli_next_result($dbConn);
    mysqli_free_result($result2);
}
/*end add by mtcc 06292012 */

$query3 = "SELECT max(ID) as max FROM cafino.tbl_terminalsessiondetails"; 
$result3= mysqli_query($dbConn, $query3);
while($row= mysqli_fetch_array($result3))
{
	$lastTransId = $row[0];
}
mysqli_next_result($dbConn);
mysqli_free_result($result3);

$lastTransId = ($lastTransId != null) ? $lastTransId : 0;
$lastTransId = ($lastTransId + 1);
$transid = 'W' . str_pad($terminalsessionid,6,0,STR_PAD_LEFT) . str_pad($lastTransId,10,0,STR_PAD_LEFT);
$query2 = "INSERT INTO cafino.tbl_terminalsessiondetails(TerminalSessionID,TransactionType,Amount,AcctID,ServiceID,TransID,ServiceTransactionID,TransDate,DateCreated) VALUES('".$terminalsessionid."','W','".$convertedbalance."','" . $_SESSION['id'] ."','1','".$transid."','$servicetransid',now_usec(),now_usec())";
//$query2 = "INSERT INTO cafino.tbl_terminalsessiondetails(TransactionType,AcctID,ServiceID,ServiceTransactionID,TransDate,DateCreated) VALUES('W','" . $_SESSION['id'] ."','1','$servicetransid',now_usec(),now_usec())";
$result2= mysqli_query($dbConn, $query2);
mysqli_next_result($dbConn);
mysqli_free_result($result2);
//Added By Arlene R. Salazar 05-30-2012

if(isset ($_SESSION['IsFreeEntry']))
{
    $query = "CALL cafino_voucher.proc_updatevoucherwinnings('$sweeps_code','$wintype[0]');";
    $result = mysqli_query($dbConn,$query);

    unset($_SESSION['IsFreeEntry']);
}
else
{
    
}

mysqli_close($dbConn);

$_SESSION['count'] = $count;

header("location: insert_regular_gaming_cards.php");

?>
