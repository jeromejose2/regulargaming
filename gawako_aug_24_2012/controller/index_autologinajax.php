<?php
session_start();
include('../../conn/connstr.php');
require_once('../include/class.rc4crypt.php');


$mac_address = $_GET['macadd'];
$ip = $_SERVER['REMOTE_ADDR'];


$query = "SELECT Name, Password, HashedPassword from tbl_terminals where MacAddress = '$mac_address';";
$result = mysqli_query($dbConn,$query);
$row = mysqli_fetch_array($result);
$sql_row_num = mysqli_num_rows($result);

$name = $row['Name'];
$pass = $row['Password'];
$hshedpass = $row['HashedPassword'];

mysqli_next_result($dbConn);
mysqli_free_result($result);

//decrypt password
$pwd = '23780984';
$hshedpass = @pack('H*', $hshedpass);
$dcrptpass = rc4crypt::decrypt($pwd, $hshedpass);
//end decrypt password

if($name == '')
{
    echo json_encode(array("msg1" => "Incorrect login for this specific terminal."));
}
else
{
    $query = "CALL proc_loginterminalregulargamingauto('$name','$pass','$ip');";
    $result = mysqli_query($dbConn,$query);
    $row = mysqli_fetch_array($result);
    $sql_row_num = mysqli_num_rows($result);

    $return_id = $row['ReturnID'];
    $return_msg = $row['ReturnMsg'];
    $id = $row['TerminalID'];
    $siteid = $row['SiteID'];
    $terminal_session_id = $row['TerminalSessionID'];

    mysqli_next_result($dbConn);
    mysqli_free_result($result);

    //insert to logs
    $filename = "../../DBLogs/logs.txt";
    $fp = fopen($filename , "a");

    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: AUTO LOGIN || ".$name." || proc_loginterminalregulargamingauto('$name','$pass','$ip') || ".$return_id." || ".$return_msg."\r\n");

    fclose($fp);

    if ($return_msg == 'Terminal login successful.')
    {
        $_SESSION['user'] = $name;
        $_SESSION['id'] = $id;
        $_SESSION['siteid'] = $siteid;
        $_SESSION['ip'] = $ip;
        $_SESSION['pass'] = $dcrptpass;
        $_SESSION['terminal_session_id'] = $terminal_session_id;

        echo json_encode(array("msg1" => "Terminal login successful."));
    }
    else if ($return_msg == 'Terminal already logged in.')
    {
        echo json_encode(array("msg1" => "Terminal already logged in."));
    }
    else
    {
        echo json_encode(array("msg1" => "Login failed. Please try again."));
    }
}

mysqli_close($dbConn);
?>
