<?php
@include("xajax_toolkit/xajax_core/xajax.inc.php");

$xajax = new xajax();

$xajax->registerFunction("GetBalance");
$xajax->registerFunction("GetBalanceConv");
$xajax->registerFunction("HeartBeat");
$xajax->registerFunction("ConvertPoints");
$xajax->registerFunction("BrowseInternet");
$xajax->registerFunction("GetRegularGamingCards");
$xajax->registerFunction("RevealCards");
$xajax->registerFunction("ViewOpenedCards");
$xajax->registerFunction("CheckSession");
$xajax->registerFunction("CheckSessionSweepsCode");
$xajax->registerFunction("EnterSweepsCode");
$xajax->registerFunction("InterBrowsingLogs");
$xajax->registerFunction("checkPendingReloadTransactions");

function GetBalance()
{
    $objResponse = new xajaxResponse();

    $login = $_SESSION['user'];
    $ip_add_MG = $_SESSION['ip'];

    include('include/MicrogamingAPI.class.php');
    include('../conn/connstr.php');
    include('../conn/config.php');

    if(isset ($_SESSION['IsFreeEntry']))
    {
        $balance = '0.10';

        $balance = "Point Balance: ".$balance;

        $objResponse->assign("balance", "innerHTML", $balance);
        $objResponse->script("toggleBalanceLoading('balance');");
        $objResponse->script("hide_loading();");
    }
    else
    {
        $query = "SELECT * FROM cafino_servicemapping.tbl_agentsessions where AgentID = 2";
        $result = mysqli_query($dbConn,$query);
        $row = mysqli_fetch_array($result);

        $sessionGUID_MG = $row['SessionGUID'];

        mysqli_next_result($dbConn);
        mysqli_free_result($result);

        mysqli_close($dbConn);


        $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
                   "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
                   "<IPAddress>".$ip_add_MG."</IPAddress>".
                   "<ErrorCode>0</ErrorCode>".
                   "<IsLengthenSession>true</IsLengthenSession>".
                   "</AgentSession>";


        $client = new nusoap_client($mg_url, 'wsdl');
        $client->setHeaders($headers);
        $param = array('delimitedAccountNumbers' => $login);
		$filename = "../APILogs/logs.txt";
        $fp = fopen($filename , "a");
        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || ".$login." || ".serialize($param)."\r\n");
        fclose($fp);
		$a = 1;
		while($a < 4)
		{
			$result = $client->call('GetAccountBalance', $param);
			//if(is_array($result))
			//{
				if(array_key_exists('faultcode', $result))
				{
					$convert = $result['faultstring'];
					$objResponse->script("popup('popUpDivLPCheckActiveSession');");
		            $objResponse->assign("msg","innerHTML",$convert);
		            $objResponse->script("hide_loading();");
					$a++;
				}
				else
				{
					if($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
					{
						$a++;
					}
					else
					{
						break;
					}
				}
			/*}
			else
			{
				$convert = "Error in getting balance.";
				$objResponse->script("popup('popUpDivLPCheckActiveSession');");
	            $objResponse->assign("msg","innerHTML",$convert);
	            $objResponse->script("hide_loading();");
				$a++;
			}*/
		}

        $balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];

        $balance = number_format($balance, 2, '.', '');
        $balance = "Point Balance: ".$balance;

        $objResponse->assign("balance", "innerHTML", $balance);
        $objResponse->script("hide_loading();");
        $objResponse->script("toggleBalanceLoading('balance');");
    }

    return $objResponse;
}

function GetBalanceConv()
{
    $objResponse = new xajaxResponse();

    $login = $_SESSION['user'];
    $ip_add_MG = $_SESSION['ip'];

    include('include/MicrogamingAPI.class.php');
    include('../conn/connstr.php');
    include('../conn/config.php');

    if(isset ($_SESSION['IsFreeEntry']))
    {
        $balance = '0.10';

        $balance = "Point Balance: ".$balance;

        $objResponse->assign("balance", "innerHTML", $balance);
        $objResponse->script("toggleBalanceLoading('balance');");
        $objResponse->script("hide_loading();");
    }
    else
    {
        $query = "SELECT * FROM cafino_servicemapping.tbl_agentsessions where AgentID = 2";
        $result = mysqli_query($dbConn,$query);
        $row = mysqli_fetch_array($result);

        $sessionGUID_MG = $row['SessionGUID'];

        mysqli_next_result($dbConn);
        mysqli_free_result($result);

        mysqli_close($dbConn);


        $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
                   "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
                   "<IPAddress>".$ip_add_MG."</IPAddress>".
                   "<ErrorCode>0</ErrorCode>".
                   "<IsLengthenSession>true</IsLengthenSession>".
                   "</AgentSession>";


        $client = new nusoap_client($mg_url, 'wsdl');
        $client->setHeaders($headers);
        $param = array('delimitedAccountNumbers' => $login);
		$filename = "../APILogs/logs.txt";
        $fp = fopen($filename , "a");
        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || ".$login." || ".serialize($param)."\r\n");
        fclose($fp);
		$a = 1;
		while($a < 4)
		{
			$result = $client->call('GetAccountBalance', $param);
			if(array_key_exists('faultcode', $result))
			{
				$convert = $result['faultstring'];
				$objResponse->script("popup('popUpDivLPCheckActiveSession');");
                $objResponse->assign("msg","innerHTML",$convert);
                $objResponse->script("hide_loading();");
				$a++;
			}
			else
			{
				if($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
				{
					$a++;
				}
				else
				{
					break;
				}
			}
		}
        
        $balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];

        $balance = number_format($balance, 2, '.', '');

        $objResponse->assign("convbal", "innerHTML", $balance);
        $objResponse->script("hide_loading();");
        $objResponse->script("popup('popUpDivLPConvert');");
    }

    return $objResponse;
}

function HeartBeat()
{
    $objResponse = new xajaxResponse();

    $login = $_SESSION['user'];
    $ip_add_MG = $_SESSION['ip'];

    include('include/MicrogamingAPI.class.php');
    include('../conn/connstr.php');
    include('../conn/config.php');


    $query = "SELECT * FROM cafino_servicemapping.tbl_agentsessions where AgentID = 2";
    $result = mysqli_query($dbConn,$query);
    $row = mysqli_fetch_array($result);

    $sessionGUID_MG = $row['SessionGUID'];

    mysqli_next_result($dbConn);
    mysqli_free_result($result);

    mysqli_close($dbConn);


    $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
               "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
               "<IPAddress>".$ip_add_MG."</IPAddress>".
               "<ErrorCode>0</ErrorCode>".
               "<IsLengthenSession>true</IsLengthenSession>".
               "</AgentSession>";


    $client = new nusoap_client($mg_url, 'wsdl');
    $client->setHeaders($headers);
    $param = array('delimitedAccountNumbers' => $login);
	$filename = "../APILogs/logs.txt";
    $fp = fopen($filename , "a");
    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || ".$login." || ".serialize($param)."\r\n");
    fclose($fp);
	$a = 1;
	while($a < 4)
	{
		$result = $client->call('GetAccountBalance', $param);
		if(array_key_exists('faultcode', $result))
		{
			$convert = $result['faultstring'];
			$objResponse->script("popup('popUpDivLPCheckActiveSession');");
            $objResponse->assign("msg","innerHTML",$convert);
            $objResponse->script("hide_loading();");
			$a++;
		}
		else
		{
			if($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
			{
				$a++;
			}
			else
			{
				break;
			}
		}
	}

    return $objResponse;
}

function CheckSession($game_id)
{
    $objResponse = new xajaxResponse();

    $id = $_SESSION['id'];
    $login = $_SESSION['user'];
    $passwrd = $_SESSION['pass'];

    include('../conn/connstr.php');

    $query = "SELECT * FROM tbl_terminalsessions where TerminalID = '$id' AND DateEnd = 0;";
    $result = mysqli_query($dbConn,$query);
    $row = mysqli_fetch_array($result);
    $sql_row_num = mysqli_num_rows($result);

    mysqli_next_result($dbConn);
    mysqli_free_result($result);

    if ($sql_row_num > 0)
    {
        if(isset ($_SESSION['IsFreeEntry']))
        {
            $convert = "No Active Session.";

            $objResponse->script("popup('popUpDivLPCheckActiveSession');");
            $objResponse->assign("msg","innerHTML",$convert);
        }
        else
        {
            if($game_id == 'keno')
            {
                $objResponse->script("window.location.href='launcher.php?gid=Keno&user=".$login."&pass=".$passwrd."'");
            }
            else if($game_id == 'qhorse1')
            {
                $objResponse->script("window.location.href='launcher.php?gid=PremierRacing&user=".$login."&pass=".$passwrd."'");
                //$objResponse->alert("Game is undergoing system enhancement. Sorry for the inconvenience.");
            }
            else if($game_id == 'qhorse2')
            {
                $objResponse->script("window.location.href='launcher.php?gid=RoyalDerby&user=".$login."&pass=".$passwrd."'");
            }
	     	else if($game_id == 'dino')
            {
                $objResponse->script("window.location.href='launcher.php?gid=DinoMight&user=".$login."&pass=".$passwrd."'");
            }
	     	else if($game_id == 'tomb')
            {
                $objResponse->script("window.location.href='launcher.php?gid=TombRaider&user=".$login."&pass=".$passwrd."'");
            }
	     	else if($game_id == 'bj')
            {
                $objResponse->script("window.location.href='launcher.php?gid=bj&user=".$login."&pass=".$passwrd."'");
            }
			else if($game_id == 'tomb2')
            {
                $objResponse->script("window.location.href='launcher.php?gid=TombRaiderII&user=".$login."&pass=".$passwrd."'");
            }
			else if($game_id == 'wheel')
            {
                $objResponse->script("window.location.href='launcher.php?gid=WheelofWealth&user=".$login."&pass=".$passwrd."'");
            }
        }

        $query = "Update tbl_terminals set ServiceID = 3 where ID = '$id';";
        $sql_result = mysqli_query($dbConn,$query);

        mysqli_close($dbConn);
    }
    else
    {
        $convert = "No Active Session.";

        $objResponse->script("popup('popUpDivLPCheckActiveSession');");
        $objResponse->assign("msg","innerHTML",$convert);
    }

    return $objResponse;
}

function CheckSessionSweepsCode()
{
    $objResponse = new xajaxResponse();

    $id = $_SESSION['id'];

    include('../conn/connstr.php');

    if(isset ($_SESSION['IsFreeEntry']))
    {
        $convert = "<p>You have an existing free entry sweeps code.</p></br/> <p>Please convert this point first.</p>";

        $objResponse->script("popup('popUpDivLPCheckActiveSession');");
        $objResponse->assign("msg","innerHTML",$convert);
    }
    else
    {
        $objResponse->script("location.href='entrycode-page.php';");
        
        /*
        $query = "SELECT * FROM tbl_terminalsessions where TerminalID = '$id' AND DateEnd = 0;";
        $result = mysqli_query($dbConn,$query);
        $row = mysqli_fetch_array($result);
        $sql_row_num = mysqli_num_rows($result);

        mysqli_next_result($dbConn);
        mysqli_free_result($result);

        mysqli_close($dbConn);

        if ($sql_row_num > 0)
        {
            $convert = "<p>You are not allowed to use this feature if you</p><br/> <p>have an active session.</p>";

            $objResponse->script("popup('popUpDivLPCheckActiveSession');");
            $objResponse->assign("msg","innerHTML",$convert);
        }
        else
        {
            
        }
         */
    }

    return $objResponse;
}

function ConvertPoints()
{
    $objResponse = new xajaxResponse();

    $arrStr = explode("/", $_SERVER['SCRIPT_NAME'] );
    $arrStr = array_reverse($arrStr );

    $login = $_SESSION['user'];
    $id = $_SESSION['id'];
    $siteid = $_SESSION['siteid'];
    $ip = $_SESSION['ip'];
    $page = $arrStr[0];
    $terminal_session_id = $_SESSION['tsi'];
    $terminalbalance = 0;

    include('include/MicrogamingAPI.class.php');
    include('../conn/connstr.php');
    include('../conn/config.php');
    include('controller/microseconds.php');
    


    if(isset ($_SESSION['IsFreeEntry']))
    {
        $query = "CALL proc_endsession('$id','$terminal_session_id');";
        $result = mysqli_query($dbConn,$query);
        $row = mysqli_fetch_array($result);
        $sql_row_num = mysqli_num_rows($result);

        $return = $row['ReturnID'];
        $return_msg = $row['ReturnMsg'];

        mysqli_next_result($dbConn);
        mysqli_free_result($result);

        if($return == 0)
        {
            $convert = "You have converted 0.10 point/s for 1 number of sweepstakes card/s. Click PROCEED to reveal your cards.";
            $okbutton = "<img src='images/ProceedButton.png' onclick=\"display_loading_img(); location.href='controller/get_regular_gaming_cards.php';\" style='cursor: pointer;' />";
        }
        else
        {
            $convert = "An Error Occured. Please try again. ".$return_msg;
            $okbutton = "<img src='images/ProceedButton.png' onclick=\"popup('popUpDivConvert');\" style='cursor: pointer;' />";
        }

        $objResponse->script("popup('popUpDivConvert');");
        $objResponse->assign("convert","innerHTML",$convert);
        $objResponse->assign("okbtn","innerHTML",$okbutton);
    }
    else
    {
        $query = "SELECT * FROM tbl_terminalsessions where TerminalID = '$id' AND DateEnd = 0;";
        $result = mysqli_query($dbConn,$query);
        $row = mysqli_fetch_array($result);
        $sql_row_num = mysqli_num_rows($result);

        mysqli_next_result($dbConn);
        mysqli_free_result($result);

        if ($sql_row_num > 0)
        {
            //added 03/23/2012 mtcc
            $query = "SELECT Balance FROM cafino.tbl_terminals WHERE ID = '$id'";
            $result = mysqli_query($dbConn,$query);
            $row = mysqli_fetch_array($result);
            mysqli_next_result($dbConn);
            mysqli_free_result($result);
            if ($sql_row_num > 0)
            {
                $terminalbalance = $row['Balance'];
            }
            //added 03/23/2012 mtcc
            
            $login = $_SESSION['user'];
            $ip_add_MG = $_SESSION['ip'];

            $query = "SELECT * FROM cafino_servicemapping.tbl_agentsessions where AgentID = 2";
            $result = mysqli_query($dbConn,$query);
            $row = mysqli_fetch_array($result);

            $sessionGUID_MG = $row['SessionGUID'];

            mysqli_next_result($dbConn);
            mysqli_free_result($result);


            $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
                       "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
                       "<IPAddress>".$ip_add_MG."</IPAddress>".
                       "<ErrorCode>0</ErrorCode>".
                       "<IsLengthenSession>true</IsLengthenSession>".
                       "</AgentSession>";


            $client = new nusoap_client($mg_url, 'wsdl');
            $client->setHeaders($headers);
            $param = array('delimitedAccountNumbers' => $login);
			if(serialize($param) != "b:0") // Added by Arlene R. Salazar 04-29-2012
            {
				$filename = "../APILogs/logs.txt";
				$fp = fopen($filename , "a");
				fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || ".$login." || ".serialize($param)."\r\n");
				fclose($fp);
				$a = 1;
				while($a < 4)
				{
					$result = $client->call('GetAccountBalance', $param);
					if(array_key_exists('faultcode', $result))
					{
						$convert = $result['faultstring'];
						$objResponse->script("popup('popUpDivLPCheckActiveSession');");
				        $objResponse->assign("msg","innerHTML",$convert);
				        $objResponse->script("hide_loading();");
						$a++;
					}
					else
					{
						if($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
						{
							$a++;
						}
						else
						{
							break;
						}
					}
				}
		        $string = serialize($result);
				if(($string == "b:0") || (count($result) == 0) || (count($result["GetAccountBalanceResult"]["BalanceResult"]) != 8)) // Added by Arlene R. Salazar 04-29-2012
                {
                    $convert = "Error in serializing Get Balance.";

                    $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                    $objResponse->assign("msg","innerHTML",$convert);
                    $objResponse->script("hide_loading();");
                }
                else
                {
				    $balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];
				    
				    //added 03/21/2012 mtcc
				    
				    $filename = "../APILogs/logs.txt";
				    $fp = fopen($filename , "a");
				    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GET TERMINAL BALANCE(CONVERT POINTS) || ".$login." || ".$string."\r\n");
				    fclose($fp);

				    //if (($balance != $terminalbalance) || $balance == null || $balance == '')
				    //{
				    //    $balance = $terminalbalance;
				    //}
				    //added 03/21/2012 mtcc

				    if ($balance >= 1)
				    {
				        //INSERT TO LOGS
				        $datestamp = udate("YmdHisu");

				        $query = "CALL proc_inserttotransactionlog('$datestamp','$id',3,1,'W','$balance',1,@retval)";
				        $result = mysqli_query($dbConn,$query);
				        $row = mysqli_fetch_array($result);

				        $return = $row['ReturnID'];
				        $returnmsg = $row['ReturnMsg'];

				        mysqli_next_result($dbConn);
				        mysqli_free_result($result);

				        //insert to logs
				        $filename = "../DBLogs/logs.txt";
				        $fp = fopen($filename , "a");

				        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || ".$login." || proc_inserttotransactionlog('$datestamp','$id',3,1,'W','$balance',1,@retval) || ".$return." || ".$returnmsg."\r\n");

				        fclose($fp);


				        if($return == 0)
				        {
				            $param_withdraw = array('accountNumber' => $login, 'amount' => $balance, 'currency' => 1);
							$filename = "../APILogs/logs.txt";
							$fp = fopen($filename , "a");
							fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: Withdrawal || ".$login." || ".serialize($param_withdraw)."\r\n");
							fclose($fp);
				            $result_withdraw = $client->call('Withdrawal', $param_withdraw);
							$string = serialize($result_withdraw);
							if(($string == "b:0") || (count($result_withdraw) == 0) || (count($result_withdraw["WithdrawalResult"]) != 8)) // Added by Arlene R. Salazar 04-29-2012
                            {
                                $convert = "Error in serializing Withdrawal.";

                                $objResponse->script("popup('popUpDivConvert');");
                                $objResponse->assign("convert","innerHTML",$convert);
                                $objResponse->assign("okbtn","innerHTML",$okbutton);
                            }
                            else
                            {
						        $errormsg = $result_withdraw["WithdrawalResult"]["IsSucceed"];
						        $externaltransid = $result_withdraw["WithdrawalResult"]["TransactionId"];
						        $transaction_amt = $result_withdraw["WithdrawalResult"]["TransactionAmount"];
						        $transaction_credit_amt = $result_withdraw["WithdrawalResult"]["TransactionCreditAmount"];
						        $credit_bal = $result_withdraw["WithdrawalResult"]["CreditBalance"];
						        $balance_mg = $result_withdraw["WithdrawalResult"]["Balance"];

						        if($errormsg == 'true')
						        {
						            

						            //insert to logs
						            $filename = "../APILogs/logs.txt";
						            $fp = fopen($filename , "a");

						            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: WITHDRAWAL(CONVERT POINTS) || ".$login." || ".$string."\r\n");

						            fclose($fp);

						            //UPDATE LOGS
						            $query = "CALL proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',1,1,@retval)";
						            $result = mysqli_query($dbConn,$query);
						            $row = mysqli_fetch_array($result);
						            //print_r($row);

						            $return = $row['ReturnID'];
						            $returnmsg = $row['ReturnMsg'];

						            mysqli_next_result($dbConn);
						            mysqli_free_result($result);

						            //insert to logs
						            $filename = "../DBLogs/logs.txt";
						            $fp = fopen($filename , "a");

						            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || ".$login." || proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',1,1,@retval) || ".$return." || ".$returnmsg."\r\n");

						            fclose($fp);

						            if($return == 0)
						            {
						                $query = "CALL proc_convertpoints('$siteid','$id','$balance')";
						                $result = mysqli_query($dbConn,$query);
						                $row = mysqli_fetch_array($result);

						                $return = $row['ReturnID'];
						                $equivalentpoints = $row['ConvertedPoints'];
						                $returnmsg = $row['ReturnMsg'];

						                mysqli_next_result($dbConn);
						                mysqli_free_result($result);

						                //insert to logs
						                $filename = "../DBLogs/logs.txt";
						                $fp = fopen($filename , "a");

						                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || ".$login." || proc_convertpoints('$siteid','$id','$balance') || ".$return." || ".$returnmsg."\r\n");

						                fclose($fp);

						                if($return == 0)
						                {
						                    $query = "Update tbl_terminals set ServiceID = 0 where ID = '$id'";
						                    $result = mysqli_query($dbConn,$query);

						                    mysqli_close($dbConn);

						                    $_SESSION['equivalentpoints'] = $equivalentpoints;

						                    $balance = number_format($balance,2);

						                    $_SESSION['convertedbalance'] = $balance; //added by mtcc on 11/02/2011

						                    $convert = "<p>You have converted ".$balance." point/s for ".$equivalentpoints."</p> <p style=\"margin-top: 10px;\">number of sweepstakes card/s. Click PROCEED</p> <p style=\"margin-top: 10px;\">to reveal your cards.</p>";
						                    $okbutton = "<img src='images/ProceedButton.png' onclick=\"display_loading_img(); location.href='controller/get_regular_gaming_cards.php';\" style='cursor: pointer;' />";

						                    $objResponse->script("popup('popUpDivConvert');");
						                    $objResponse->script("hide_loading();");
						                    $objResponse->assign("convert","innerHTML",$convert);
						                    $objResponse->assign("okbtn","innerHTML",$okbutton);
						                }
						                else
						                {
						                    //rollback withdraw API (deposit API amt=$balance)

						                    $convert = "An Error Occured. Please try again.(Error proc_convert_points)".$returnmsg;
						                    $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

						                    $objResponse->script("popup('popUpDivConvert');");
						                    $objResponse->assign("convert","innerHTML",$convert);
						                    $objResponse->assign("okbtn","innerHTML",$okbutton);
						                }
						            }
						            else
						            {
						                $convert = "An Error Occured. Please try again.(Error proc_updatetransactionlog)".$returnmsg;
						                $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

						                $objResponse->script("popup('popUpDivConvert');");
						                $objResponse->assign("convert","innerHTML",$convert);
						                $objResponse->assign("okbtn","innerHTML",$okbutton);
						            }
						        }
						        else
						        {
						            $string = serialize($result_withdraw);

						            //insert to logs
						            $filename = "../APILogs/logs.txt";
						            $fp = fopen($filename , "a");

						            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: WITHDRAWAL(CONVERT POINTS) || ".$login." || ".$string."\r\n");

						            fclose($fp);

						            //UPDATE LOGS
						            $query = "CALL proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',2,1,@retval)";
						            $result = mysqli_query($dbConn,$query);
						            $row = mysqli_fetch_array($result);

						            $return = $row['ReturnID'];
						            $returnmsg = $row['ReturnMsg'];

						            mysqli_next_result($dbConn);
						            mysqli_free_result($result);

						            mysqli_close($dbConn);

						            //insert to logs
						            $filename = "../DBLogs/logs.txt";
						            $fp = fopen($filename , "a");

						            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || ".$login." || proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',2,1,@retval) || ".$return." || ".$returnmsg."\r\n");

						            fclose($fp);

						            $convert = "An Error Occured. Please try again.(Withdrawal API error)";
						            $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

						            $objResponse->script("popup('popUpDivConvert');");
						            $objResponse->assign("convert","innerHTML",$convert);
						            $objResponse->assign("okbtn","innerHTML",$okbutton);
						        }
							}
							//here 2
				        }
				        else
				        {
				            $convert = "An Error Occured. Please try again.(Error proc_inserttotransactionlog)".$returnmsg;
				            $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

				            $objResponse->script("popup('popUpDivConvert');");
				            $objResponse->assign("convert","innerHTML",$convert);
				            $objResponse->assign("okbtn","innerHTML",$okbutton);
				        }
				    }
				    else
				    {
				        //INSERT TO LOGS
				        $datestamp = udate("YmdHisu");

				        $query = "CALL proc_inserttotransactionlog('$datestamp','$id',3,1,'W','$balance',1,@retval)";
				        $result = mysqli_query($dbConn,$query);
				        $row = mysqli_fetch_array($result);

				        $return = $row['ReturnID'];
				        $returnmsg = $row['ReturnMsg'];

				        mysqli_next_result($dbConn);
				        mysqli_free_result($result);

				        //insert to logs
				        $filename = "../DBLogs/logs.txt";
				        $fp = fopen($filename , "a");

				        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || ".$login." || proc_inserttotransactionlog('$datestamp','$id',3,1,'W','$balance',1,@retval) || ".$return." || ".$returnmsg."\r\n");

				        fclose($fp);


				        if($return == 0)
				        {
				            $param_withdraw = array('accountNumber' => $login, 'amount' => $balance, 'currency' => 1);
							$filename = "../APILogs/logs.txt";
							$fp = fopen($filename , "a");
							fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: Withdrawal || ".$login." || ".serialize($param_withdraw)."\r\n");
							fclose($fp);
				            $result_withdraw = $client->call('Withdrawal', $param_withdraw);
							$string = serialize($result_withdraw);
							if(($string == "b:0") || (count($result_withdraw) == 0) || (count($result_withdraw["WithdrawalResult"]) != 8)) // Added by Arlene R. Salazar 04-29-2012
                            {
                                $convert = "Error in serializing Withdrawal.";

                                $objResponse->script("popup('popUpDivConvert');");
                                $objResponse->assign("convert","innerHTML",$convert);
                                $objResponse->assign("okbtn","innerHTML",$okbutton);
                            }
                            else
                            {
						        $errormsg = $result_withdraw["WithdrawalResult"]["IsSucceed"];
						        $externaltransid = $result_withdraw["WithdrawalResult"]["TransactionId"];
						        $transaction_amt = $result_withdraw["WithdrawalResult"]["TransactionAmount"];
						        $transaction_credit_amt = $result_withdraw["WithdrawalResult"]["TransactionCreditAmount"];
						        $credit_bal = $result_withdraw["WithdrawalResult"]["CreditBalance"];
						        $balance_mg = $result_withdraw["WithdrawalResult"]["Balance"];

						        $balance_orig = $balance;
						        $balance = 1;

						        if($errormsg == 'true')
						        {
						            

						            //insert to logs
						            $filename = "../APILogs/logs.txt";
						            $fp = fopen($filename , "a");

						            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: WITHDRAWAL(CONVERT POINTS) || ".$login." || ".$string."\r\n");

						            fclose($fp);

						            //UPDATE LOGS
						            $query = "CALL proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',1,1,@retval)";
						            $result = mysqli_query($dbConn,$query);
						            $row = mysqli_fetch_array($result);

						            $return = $row['ReturnID'];
						            $returnmsg = $row['ReturnMsg'];

						            mysqli_next_result($dbConn);
						            mysqli_free_result($result);

						            //insert to logs
						            $filename = "../DBLogs/logs.txt";
						            $fp = fopen($filename , "a");

						            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || ".$login." || proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',1,1,@retval) || ".$return." || ".$returnmsg."\r\n");

						            fclose($fp);

						            if($return == 0)
						            {
						                $query = "CALL proc_convertpoints('$siteid','$id','$balance')";
						                $result = mysqli_query($dbConn,$query);
						                $row = mysqli_fetch_array($result);

						                $return = $row['ReturnID'];
						                $equivalentpoints = $row['ConvertedPoints'];
						                $returnmsg = $row['ReturnMsg'];

						                mysqli_next_result($dbConn);
						                mysqli_free_result($result);

						                //insert to logs
						                $filename = "../DBLogs/logs.txt";
						                $fp = fopen($filename , "a");

						                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || ".$login." || proc_convertpoints('$siteid','$id','$balance') || ".$return." || ".$returnmsg."\r\n");

						                fclose($fp);

						                if($return == 0)
						                {
						                    $query = "Update tbl_terminals set ServiceID = 3 where ID = '$id'";
						                    $result = mysqli_query($dbConn,$query);

						                    mysqli_close($dbConn);

						                    $_SESSION['equivalentpoints'] = $equivalentpoints;

						                    $balance = number_format($balance,2);

						                    $_SESSION['convertedbalance'] = $balance_orig; //added by mtcc on 11/02/2011

						                    $convert = "<p>You have converted ".$balance_orig." point/s for ".$equivalentpoints."</p> <p style=\"margin-top: 10px;\">number of sweepstakes card/s. Click PROCEED</p> <p style=\"margin-top: 10px;\">to reveal your cards.</p>";
						                    $okbutton = "<img src='images/ProceedButton.png' onclick=\"display_loading_img(); location.href='controller/get_regular_gaming_cards.php';\" style='cursor: pointer;' />";

						                    $objResponse->script("popup('popUpDivConvert');");
						                    $objResponse->assign("convert","innerHTML",$convert);
						                    $objResponse->assign("okbtn","innerHTML",$okbutton);
						                }
						                else
						                {
						                    //rollback withdraw API (deposit API amt=$balance)

						                    $convert = "An Error Occured. Please try again.(Error proc_convert_points)".$returnmsg;
						                    $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

						                    $objResponse->script("popup('popUpDivConvert');");
						                    $objResponse->assign("convert","innerHTML",$convert);
						                    $objResponse->assign("okbtn","innerHTML",$okbutton);
						                }
						}
						else
						{
						                $convert = "An Error Occured. Please try again.(Error proc_updatetransactionlog)".$returnmsg;
						                $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

						                $objResponse->script("popup('popUpDivConvert');");
						                $objResponse->assign("convert","innerHTML",$convert);
						                $objResponse->assign("okbtn","innerHTML",$okbutton);
						}
						        }
						        else
						        {
						            $string = serialize($result_withdraw);

						            //insert to logs
						            $filename = "../APILogs/logs.txt";
						            $fp = fopen($filename , "a");

						            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: WITHDRAWAL(CONVERT POINTS) || ".$login." || ".$string."\r\n");

						            fclose($fp);

						            //UPDATE LOGS
						            $query = "CALL proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',2,1,@retval)";
						            $result = mysqli_query($dbConn,$query);
						            $row = mysqli_fetch_array($result);

						            $return = $row['ReturnID'];
						            $returnmsg = $row['ReturnMsg'];

						            mysqli_next_result($dbConn);
						            mysqli_free_result($result);

						            mysqli_close($dbConn);

						            //insert to logs
						            $filename = "../DBLogs/logs.txt";
						            $fp = fopen($filename , "a");

						            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || ".$login." || proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',2,1,@retval) || ".$return." || ".$returnmsg."\r\n");

						            fclose($fp);

						            $convert = "An Error Occured. Please try again.(Withdrawal API error)";
						            $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

						            $objResponse->script("popup('popUpDivConvert');");
						            $objResponse->assign("convert","innerHTML",$convert);
						            $objResponse->assign("okbtn","innerHTML",$okbutton);
						        }
							}
							//here 3
				        }
				        else
				        {
				            $convert = "An Error Occured. Please try again.(Error proc_inserttotransactionlog)".$returnmsg;
				            $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

				            $objResponse->script("popup('popUpDivConvert');");
				            $objResponse->assign("convert","innerHTML",$convert);
				            $objResponse->assign("okbtn","innerHTML",$okbutton);
				        }
				    }
				}
				//here 4
			}
            else
            {
                $convert = "Error in serializing Get Balance.";

                $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                $objResponse->assign("msg","innerHTML",$convert);
                $objResponse->script("hide_loading();");
            }
			//here
        }
        else
        {
            $convert = "No Active Session.";

            $objResponse->script("popup('popUpDivLPCheckActiveSession');");
            $objResponse->assign("msg","innerHTML",$convert);
            $objResponse->script("hide_loading();");
        }
    }

    return $objResponse;
}

function BrowseInternet()
{
    $objResponse = new xajaxResponse();

    $login = $_SESSION['user'];
    $ip = $_SESSION['ip'];
    $id = $_SESSION['id'];
    $ip_add_MG = $_SESSION['ip'];
    $time_now = date('Y-m-d g:i:s');

    
	$objResponse->script("openurl('http://192.168.20.8:8105/WebBrowserProxy/upload/index.php?id=".$_SESSION['id']."&user=".$_SESSION['user']."'); location.href='launchpad.php';");
                    
    return $objResponse;
}

function RevealCards($i)
{
    $objResponse = new xajaxResponse();

    $login = $_SESSION['user'];
    $id = $_SESSION['id'];
    $siteid = $_SESSION['siteid'];
    $ip = $_SESSION['ip'];
    $resultmsg2 = $_SESSION['$resultmsg[0]'];

    include('../conn/connstr.php');

    $result= mysqli_query($dbConn, "SELECT * from tbl_winnings_dump where FK_TerminalID = '$id' AND IsOpened = 'N';");

    $count = 0;

    while ($array= mysqli_fetch_array($result))
    {
        $count++;
    }

    mysqli_next_result($dbConn);
    mysqli_free_result($result);


    $avail_cards = $count - 1;
    $avail_cards = "<div class=\"countBox_body\"><p>".$avail_cards."</p></div>";


    if($count > 0)
    {
        if($count == 1)
        {
            $resultmsg = $resultmsg2;
            $okbtn = "<div class=\"sweeps_quick7\" onclick=\"window.location.href='logout.php'\" ></div>";
            $quick_pick_btn = "<div class=\"sweeps_quick2\" ></div>";

            $objResponse->script("document.getElementById(\"view_next_set\").style.visibility = \"hidden\";");
            $objResponse->assign("quick_pick_btn", "innerHTML", $quick_pick_btn);
            $objResponse->assign("resultmsg", "innerHTML", $resultmsg);
            $objResponse->assign("okbtn", "innerHTML", $okbtn);
        }


        $query = "SELECT * from tbl_winnings_dump where FK_TerminalID = '$id' AND IsOpened = 'N';";
        $result = mysqli_query($dbConn,$query);
        $row = mysqli_fetch_array($result);
        $sql_row_num = mysqli_num_rows($result);

        $win_type = $row['WinType'];
        $ecn = $row['ECN'];
        $id = $row['ID'];

        mysqli_next_result($dbConn);
        mysqli_free_result($result);


        $query = "Update tbl_winnings_dump set IsOpened = 'Y' where ID = '$id'";
        $sql_result = mysqli_query($dbConn,$query);
        $error = mysql_error();

        mysqli_close($dbConn);


        $balance = "<div class=\"win_container2\"><p>".$win_type."</p><span>".$ecn."</span></div>";


        $objResponse->assign(".$i.", "innerHTML", $balance);
        $objResponse->assign("avail_cards", "innerHTML", $avail_cards);
    }
    else
    {
        $balance = "<div class=\"win_container2\"><div class=\"flipCard\"></div></div>";
        $objResponse->assign(".$i.", "innerHTML", $balance);

        //$objResponse->script("document.getElementById('light3').style.display='block';document.getElementById('fade').style.display='block'");
        $objResponse->alert("You do not have available scratch card(s)");
    }

    return $objResponse;
}

function ViewOpenedCards()
{
    $objResponse = new xajaxResponse();

    $id = $_SESSION['id'];

    include('../conn/connstr.php');


    $result= mysqli_query($dbConn, "SELECT * from tbl_winnings_dump where FK_TerminalID = '$id' AND IsOpened = 'Y';");

    $count = 0;

    $opened_cards = "<div style='margin-left: 35px;'>";

    while ($array= mysqli_fetch_array($result))
    {
        $win_type[$count] = $array['WinType'];
        $ecn[$count] = $array['ECN'];

        $opened_cards .= "<div class=\"sweepsContainer\"><div class=\"win_container\"><p>".$win_type[$count]."</p><span>".$ecn[$count]."</span></div></div>";

        $count++;
    }

    $opened_cards .= "</div>";

    mysqli_close($dbConn);

    $objResponse->assign("opened_cards", "innerHTML", $opened_cards);

    return $objResponse;
}

function EnterSweepsCode($sweeps_code)
{
    $objResponse = new xajaxResponse();

    $id = $_SESSION['id'];
    $ip = $_SESSION['ip'];
    $_SESSION['sweeps_code'] = $sweeps_code;
    $login = $_SESSION['user'];

    include('include/MicrogamingAPI.class.php');
    include('../conn/connstr.php');
    include('../conn/config.php');
    include('controller/microseconds.php');

    $query = "SELECT A.Status, A.EntryType, B.Description FROM cafino_voucher.tbl_voucherinfo A
            INNER JOIN cafino_voucher.tbl_denominationref B ON A.Denomination = B.ID
            WHERE VoucherNo = '$sweeps_code';";

    $result = mysqli_query($dbConn,$query);
    $row = mysqli_fetch_array($result);
    $sql_row_num = mysqli_num_rows($result);

    $status = $row['Status'];
    $entrytype = $row['EntryType'];
    $denomination = $row['Description'];

    mysqli_next_result($dbConn);
    mysqli_free_result($result);


    if($status == 2)
    {
        $title = "USED CODE";
        $msg = "The Sweeps Code you have entered has already been used.";
        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
    }
    else if($status == 3)
    {
        $title = "EXPIRED CODE";
        $msg = "The Sweeps Code you have entered has already expired.";
        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
    }
    else if($status == 4)
    {
        if($entrytype == 3)
        {
            //deposit to MG
            $query = "SELECT * FROM cafino_servicemapping.tbl_agentsessions where AgentID = 2";
            $result = mysqli_query($dbConn,$query);
            $row = mysqli_fetch_array($result);

            $sessionGUID_MG = $row['SessionGUID'];

            mysqli_next_result($dbConn);
            mysqli_free_result($result);


            $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
                       "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
                       "<IPAddress>".$ip."</IPAddress>".
                       "<ErrorCode>0</ErrorCode>".
                       "<IsLengthenSession>true</IsLengthenSession>".
                       "</AgentSession>";


            $client = new nusoap_client($mg_url, 'wsdl');
            $client->setHeaders($headers);
            $param = array('accountNumber' => $login, 'amount' => $denomination, 'currency' => 1);
			$filename = "../APILogs/logs.txt";
			$fp = fopen($filename , "a");
			fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: Deposit || ".$login." || ".serialize($param)."\r\n");
			fclose($fp);
    	    $result = $client->call('Deposit', $param);

            $errormsg = $result['DepositResult']['IsSucceed'];
            $transid_mg = $result["DepositResult"]["TransactionId"];
            $acctno_mg = $result["DepositResult"]["AccountNumber"];
            $transamt_mg = $result["DepositResult"]["TransactionAmount"];
            

            $datestamp = udate("Y-m-d H:i:s.u");


            if($errormsg == 'true')
            {
                $string = serialize($result);

                //insert to logs
                $filename = "../APILogs/logs.txt";
                $fp = fopen($filename , "a");

                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: DEPOSIT || ".$string."\r\n");

                fclose($fp);

                $query = "Update cafino_voucher.tbl_voucherinfo set Status = 2, DateUsed = '$datestamp', TerminalID = '$id' where VoucherNo = '$sweeps_code';";
                $sql_result = mysqli_query($dbConn,$query);

                $query = "SELECT * FROM tbl_terminalsessions where TerminalID = '$id' AND DateEnd = 0;";
                $result = mysqli_query($dbConn,$query);
                $row = mysqli_fetch_array($result);
                $sql_row_num = mysqli_num_rows($result);

                mysqli_next_result($dbConn);
                mysqli_free_result($result);

                if ($sql_row_num > 0)
                {
                    //RELOAD

                    //start session
                    $query = "CALL proc_reload_noncashier('$id','$denomination','$ip','$transid_mg');";
                    $result = mysqli_query($dbConn,$query);
                    $row = mysqli_fetch_array($result);
                    $sql_row_num = mysqli_num_rows($result);

                    $return = $row['ReturnID'];
                    $return_msg = $row['ReturnMsg'];

                    mysqli_next_result($dbConn);
                    mysqli_free_result($result);

                    mysqli_close($dbConn);

                    //insert to logs
                    $filename = "../DBLogs/logs.txt";
                    $fp = fopen($filename , "a");

                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: ENTER SWEEPS CODE || ".$login." || proc_reload_noncashier('$id','$denomination','$ip','$transid_mg') || ".$return." || ".$returnmsg."\r\n");

                    fclose($fp);

                    if($return == 0)
                    {
                        //$title = "CREDIT POINTS";
						$title = "SUCCESSFUL RELOAD";
                        //$msg = "You have successfully loaded ".$denomination." point(s).";
						$msg = "You have successfully reloaded ".$denomination." points using your voucher";
                        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"window.location.reload( true ); location.href='launchpad.php'\" style=\"cursor:pointer;\"/>";
                    }
                    else
                    {
                        //WITHDRAW from MG API
                        $title = "ERROR";
                        $msg = "An Error Occured. Please try again. ".$return_msg;
                        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                    }
                    
                }
                else
                {
                    //INITIAL DEPOSIT

                    //start session
                    $query = "CALL proc_startsession_noncashier('$id','$denomination','$ip','$transid_mg');";
                    $result = mysqli_query($dbConn,$query);
                    $row = mysqli_fetch_array($result);
                    $sql_row_num = mysqli_num_rows($result);

                    $return = $row['ReturnID'];
                    $return_msg = $row['ReturnMsg'];

                    mysqli_next_result($dbConn);
                    mysqli_free_result($result);

                    mysqli_close($dbConn);

                    //insert to logs
                    $filename = "../DBLogs/logs.txt";
                    $fp = fopen($filename , "a");

                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: ENTER SWEEPS CODE || ".$login." || proc_startsession_noncashier('$id','$denomination','$ip','$transid_mg') || ".$return." || ".$returnmsg."\r\n");

                    fclose($fp);

                    if($return == 0)
                    {
                        //$title = "CREDIT POINTS";
						$title = "SUCCESSFUL INITIAL DEPOSIT";
                        $msg = "You have successfully loaded ".$denomination." points using your voucher. You may use these points to Browse the Internet or Play Games";
                        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"window.location.reload( true ); location.href='launchpad.php'\" style=\"cursor:pointer;\"/>";
                    }
                    else
                    {
                        //WITHDRAW from MG API
                        $title = "ERROR";
                        $msg = "An Error Occured. Please try again. ".$return_msg;
                        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                    }
                }
            }
            else
            {
                mysqli_close($dbConn);

                $string = serialize($result);

                //insert to logs
                $filename = "../APILogs/logs.txt";
                $fp = fopen($filename , "a");

                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: DEPOSIT || ".$string."\r\n");

                fclose($fp);
                
                $title = "ERROR";
                $msg = "An Error Occured. Please try again. API Deposit Error.";
                $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
            }


        }
        else
        {
            //check if there is an existing session
            $query = "SELECT * FROM tbl_terminalsessions where TerminalID = '$id' AND DateEnd = 0;";
            $result = mysqli_query($dbConn,$query);
            $row = mysqli_fetch_array($result);
            $sql_row_num = mysqli_num_rows($result);

            mysqli_next_result($dbConn);
            mysqli_free_result($result);

            if ($sql_row_num > 0)
            {
                //$title = "ERROR";
				$title = "FREE ENTRY NOT ALLOWED";
                //$msg = "You currently have an active session. Entering of a Free Sweeps Code is not allowed.";
				$msg = "Sorry, but you are not allowed to enter a Free Entry code with an ongoing session.";
                $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
            }
            else
            {
		  $title = "ERROR";
                $msg = "Please enter your Free Entry Sweeps Code on the designated Free Entry terminal.";
                $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"location.href='launchpad.php'\" style=\"cursor:pointer;\"/>";


/*
                $_SESSION['IsFreeEntry'] = 'y';

                //get cashier session id
                $query = "SELECT * FROM tbl_accountsessions where DateEnd IS NULL ORDER BY ID desc LIMIT 1;";

                $result = mysqli_query($dbConn,$query);
                $row = mysqli_fetch_array($result);
                $sql_row_num = mysqli_num_rows($result);

                $session_id = $row['SessionID'];

                mysqli_next_result($dbConn);
                mysqli_free_result($result);



                //start free entry session
                $query = "CALL proc_startsessionfreeentry('$id',0.10,'$session_id');";
                $result = mysqli_query($dbConn,$query);
                $row = mysqli_fetch_array($result);
                $sql_row_num = mysqli_num_rows($result);

                $return = $row['ReturnID'];
                $return_msg = $row['ReturnMsg'];
                $terminal_session_id = $row['TerminalSessionID'];

                mysqli_next_result($dbConn);
                mysqli_free_result($result);

                mysqli_close($dbConn);

                $_SESSION['terminal_session_id'] = $terminal_session_id;

                if($return == 0)
                {
                    $title = "CREDIT POINTS";
                    $msg = "You have 0.10 points. Please convert this points to avail your sweepstakes entry.";
                    $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"location.href='launchpad.php'\" style=\"cursor:pointer;\"/>";
                }
                else
                {
                    $title = "ERROR";
                    $msg = "An Error Occured. Please try again. ".$return_msg;
                    $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                }
*/
            }
        }
    }
    else
    {
        $title = "INVALID CODE";
        $msg = "You have entered an invalid sweeps code. Please check if you entered the code correctly.";
        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
    }

    $objResponse->script("hide_loading();");
    $objResponse->assign("title", "innerHTML", $title);
    $objResponse->assign("msg", "innerHTML", $msg);
    $objResponse->assign("okbtn", "innerHTML", $okbtn);
    $objResponse->script("popup('popUpDivEntryCode');");



    return $objResponse;
}

function InterBrowsingLogs($btn_name)
{
    $objResponse = new xajaxResponse();

    $login = $_SESSION['user'];

    if($btn_name == 'browse_btn')
    {
        //insert to logs
        $filename = "../BrowseInternetLogs/logs.txt";
        $fp = fopen($filename , "a");

        fwrite($fp, date("Y-m-d H:i:s") . " || ".$login." || BUTTON CLICKED: INTERNET BROWSING BUTTON || \r\n");

        fclose($fp);
    }
    else if($btn_name == 'cancel_btn')
    {
        //insert to logs
        $filename = "../BrowseInternetLogs/logs.txt";
        $fp = fopen($filename , "a");

        fwrite($fp, date("Y-m-d H:i:s") . " || ".$login." || BUTTON CLICKED: INTERNET BROWSING CANCEL BUTTON || \r\n");

        fclose($fp);
    }
    else
    {
        //insert to logs
        $filename = "../BrowseInternetLogs/logs.txt";
        $fp = fopen($filename , "a");

        fwrite($fp, date("Y-m-d H:i:s") . " || ".$login." || BUTTON CLICKED: INTERNET BROWSING CONFIRMATION BUTTON || \r\n");

        fclose($fp);
    }

    return $objResponse;
}

/*Created By: Arlene R. Salazar 03/20/2012*/
function checkPendingReloadTransactions()
{
	include('../conn/connstr.php');

	$objResponse = new xajaxResponse();

	$query = "SELECT * FROM tbl_transactionlogs WHERE Status = '0' AND TransactionType = 'R' AND TerminalID = '" . $_SESSION['id'] . "' AND DateCreated >= date_sub('" .date("Y-m-d"). "', INTERVAL 2 DAY)";
    $result = mysqli_query($dbConn,$query);
	$sql_row_num = mysqli_num_rows($result);
	if($sql_row_num > 0)
	{
		$message = '<div class="convertPoints" style="cursor: default;" onclick="javascript: alert(\'You cannot convert your points because you still have a pending reload transaction.\');">
	                    <img src="images/convertPoints.png" height="33px" width="150px" />
	                </div>';
	}
	else
	{
		$message = '<div class="convertPoints" onclick="show_loading(); xajax_GetBalanceConv();">
	                    <img src="images/convertPoints.png" height="33px" width="150px" />
	                </div>';
	}
	$objResponse->assign("convertPointsContainer", "innerHTML", $message);

	return $objResponse;
}
/*Created By: Arlene R. Salazar 03/20/2012*/

$xajax->processRequest();
$xajax_js = $xajax->getJavascript("xajax_toolkit");
?>
