$(document).ready(function(){

	var shifton = false;
    var pos = 0;

	// toggles the keyboard to show or hide when link is clicked

	$("#showkeyboard").click(function(e) {
		var height = $('#keyboard').height();
		var width = $('#keyboard').width();
		leftVal=e.pageX-600+"px";
		topVal=e.pageY+30+"px";
		$('#keyboard').css({left:leftVal,top:topVal}).toggle();

	});



	// makes the keyboard draggable
	//$("#keyboard").draggable();

	// toggles between the normal and the "SHIFT keys" on the keyboard
	function onShift(e)
	{
		var i;
		if(e==1)
		{
			for(i=1;i<4;i++)
			{
				var rowid = "#row" + i;
				$(rowid).hide();
				$(rowid+"_shift").show();
			}
		}
		else {
			for(i=1;i<4;i++) {
				var rowid = "#row" + i;
				$(rowid).show();
				$(rowid+"_shift").hide();
			}
		}
	 }

	// function thats called when any of the keys on the keyboard are pressed


	$("#keyboard input").bind("click", function(e) {

		if( $(this).val() == 'Backspace' ) {
			$('#txtVoucher').replaceSelection("", true);
            // get cursor position
            pos = document.getElementById("txtVoucher").selectionStart;
	 	}

		else if( $(this).val() == "Shift" ) {
			if(shifton == false) {
				onShift(1);
				shifton = true;
			}

			else {
				onShift(0);
				shifton = false;
			}
		}
		else {
            // clear all
            if($(this).attr('id') == 'clearall') {
               $('#txtVoucher').val('');
            // move cursor to left
            } else if($(this).attr('id') == 'leftarrow') {
               pos = pos - 1
               setCaretToPos(document.getElementById("txtVoucher"),pos);
            // move cursor to right
            } else if($(this).attr('id') == 'rightarrow') {
               pos = pos + 1;
               setCaretToPos(document.getElementById("txtVoucher"),pos);
            } else {
               $('#txtVoucher').replaceSelection($(this).val(), true);
               // get cursor position
               pos = document.getElementById("txtVoucher").selectionStart;
            }


			if(shifton == true) {
				//onShift(0);
				shifton = false;
			}
		}

	});

    /**
     * Author: Bryan Salazar
     * Date Creation: May 31, 2011
     * Description: get cursor position when textbox is click
     */
    $('#txtVoucher').click(function(){
        // $(this).selectionStart or $('#txtVoucher').selectionStart
        //  is not working
        pos = document.getElementById("txtVoucher").selectionStart;
    })

    /**
     * Author: Bryan Salazar
     * Date Creation: May 31, 2011
     * Description: Set cursor position
     */
    function setCaretToPos (input, selStart) {
        selectionEnd = selStart;
        if (input.setSelectionRange) {
            input.focus();
            input.setSelectionRange(selStart, selectionEnd);
        }
        else if (input.createTextRange) {
            var range = input.createTextRange();
            range.collapse(true);
            range.moveEnd('character', selectionEnd);
            range.moveStart('character', selectionStart);
            range.select();
        }
    }

});




