<?php error_reporting(E_ALL);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login : Launch Pad</title>
<link rel="stylesheet" type="text/css" href="css/login.css" />
<style>
    .txtName2{
	font: bold 20px/25px "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	color: #FFF;
	text-transform: uppercase;
	width:150px;
	height:auto;
	margin:10px 0 0 15px;
	float:left;
        width: 100%;
        text-align: center;
    }
</style>
</head>
<?php 
include("../connstr.php");

$query = "SELECT * FROM tbl_managelogin";
if ($result = mysqli_query($dbConn, $query)) {
    while ($row = mysqli_fetch_row($result))
    {
        $enablemanual = $row[2];
        $enableregister = $row[3];
    }
}

mysqli_next_result($dbConn);
mysqli_free_result($result);
mysqli_close($dbConn);
?>
<body>
<div id="mainContainer">
    <div id="banner"></div>
    <div id="contentContainer">
        <div id="Login_Cont">
            <div class="login_logo"></div>
            <div class="login_body">
                <div class="loginCont_body">
                    <div class="txtName2">CHOOSE LOGIN TYPE:</div>
                     <div style="width: 100%; text-align: center;">
			   <img src="images/autologin.png" width="150" height="40" style="margin-top: 5px; cursor: pointer;" onclick="window.location.href='index_autologin.php'" />
                           <?php if ($enablemanual == 1) { ?>
			   <img src="images/manuallogin.png" width="150" height="40" style="margin-top: 5px; cursor: pointer;" onclick="window.location.href='index_manuallogin.php'" />
                           <?php } else {?>
                           <img src="images/manuallogin.png" width="150" height="40" style="margin-top: 5px;" />
                           <?php } ?>
                           
                    </div>
		      <div style="width: 100%; text-align: center;">
                          <?php if ($enableregister == 1) { ?>
			   <img src="images/registerred.png" width="180" height="40" style="margin-top: 10px; cursor: pointer;" onclick="window.location.href='index_register.php'" />
                          <?php } else {?>
                           <img src="images/registerred.png" width="180" height="40" style="margin-top: 10px;"/>
                          <?php } ?> 
		      </div>
                </div>
                <div class="login_bottom">&nbsp;</div>
                <div id="err_msg" align="center"><?php include('controller/cindex.php'); ?></div>
            </div>
        </div>
        <div id="footer"></div>
    </div>
</div>
</body>
</html>
