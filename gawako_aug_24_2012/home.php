<?php
session_start();
include('controller/trans.php');
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Landing Page</title>
<link rel="stylesheet" type="text/css" href="css/main.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
<link href="css/lightbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="js/CSSPopUp.js"></script>
<script src="js/jquery-1.4.1.js" type="text/javascript"></script>
<script src="js/jquery.background.image.scale-0.1.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="js/form_validation.js"></script>
<script language="javascript" type="text/javascript" src="js/ypSimpleScroll.js"></script>
<script language="javascript" type="text/javascript">
    var test  = new ypSimpleScroll("myScroll", 20, 50, 150, 250, 100, 200)
</script>
<script type="text/javascript">
        //Using document.ready causes issues with Safari when the page loads
        jQuery(window).load(function(){
                $("#contentContainer").backgroundScale({
                        imageSelector: "#gaBG",
                        centerAlign: true,
                        containerPadding: 0
                });
        });
</script>

<script type="text/javascript">
function heartbeat()
{
    xajax_HeartBeat();
}

setInterval ("heartbeat()", 600000);
</script>

<?php $xajax->printJavascript(); ?>
</head>

<body onload="test.load(); document.getElementById('light3').style.display='block';document.getElementById('fade').style.display='block';">
<div id="blanket" style="display:none;"></div>
<div id="popUpDivHome" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 60px;">TERMS AND CONDITION CONFIRMATION</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="convert" align="center"><p>Please confirm that you have read the</p><br/><p> Terms & Conditions.</p></div>
        <div id="okbtn" align="center" style="margin-top: 40px;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivHome');" style="cursor:pointer;"/></div>
    </div>
</div>

<div id="light" class="white_content"><?php include('mechanics.php') ?></div>
<div id="light2" class="white_content"><?php include('terms.php') ?></div>
<div id="light3" class="white_content"><?php include('terms2.php') ?></div>
<div id="fade" class="black_overlay"></div>

    <div id="mainContainer">
    	<div id="banner">
            <div id="logo_landing"> <img src="images/theSweepsLogo.png" alt="" /></div>
    	</div>
        <div id="contentContainer" style="height: 63%; top: 210px;">
        <form method="post" action="launchpad.php" onsubmit="">
            <img id="gaBG" src="images/contentbg2.jpg" height="577px" alt="" />
            <div id="about_container">
                <div id="myScrollContainer" style="width:750px; height:400px; margin-left: 130px; margin-top: -40px;">
                    <div align="justify" id="myScrollContent" style="width:750px; height:380px;">
                        <div class="aboutTitle">WELCOME TO THE SWEEPS CENTER!</div>
                        <br/><br/><br/>
                        <div class="about">
				<br/>
    		              <div align="center" style="font-weight: bold; font-style: italic; font-size: 25px;">NO PURCHASE NEEDED!</div>
                            <!--<div align="center" style="font-weight: bold; font-style: italic;">can give you a chance to win lots of prizes!</div>-->
                            <br/><br/>
                            <div align="justify">The Sweeps Center is a one-stop managed sweepstakes shop that sells a wide variety of products.  For every purchase you make, you get a chance to win up to US$ 3,000 cash instantly!</div>
                            <br/>
                            <div align="justify">Here at The Sweeps Center, the products you buy can be claimed at participating retail outlets.  Or you can also convert it to points which you can use to browse the internet or participate in our Sweepstakes service via our Sweeps Games.  The points accumulated after your session will be then converted to e - Sweeps card entries, giving you the chance to win lots of different prizes. </div>
                            <!--<img src="images/welcome_img.png" width="750" alt="" />-->
                            <br/>
                            <div align="justify">Claiming of prizes won&apos;t be a hassle as you can get your winnings right there and then.</div>
                            
                            <img src="images/welcome_img_lp.png" width="750" height="70" alt="" />
                            <br/><br/>
                            <div align="justify">Visit the Sweeps Center at (location address) for a higher quality of Sweepstakes service that is more fun and rewarding! </div>
                            <br/>
                            <!--<div align="center"><input type="checkbox" id="terms" name="terms" /> I have read and agreed with Sweeps Center’s <label style="cursor: pointer; color: blue;" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'"><u>Terms and Conditions.</u></label></div>-->
                        </div>
                        
                    </div>
                 </div>
                <div class="goToLaunchPad_quickContainer">
                            <input type="submit" class="gotToLaunchpad" value="" />
                </div>
            </form>
            </div>
            <div style="float: left; margin-left: 10px; margin-top: 10px;">
                <label style="cursor: pointer;" onmouseover="test.scrollNorth()" onmouseout="test.endScroll()"><img src="images/scroll_up.png" alt="" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none"  /></label>
                <br /><br />
                <label style="cursor: pointer;" onmouseover="test.scrollSouth()" onmouseout="test.endScroll()"><img src="images/scroll_down.png" alt="" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none"  /></label>
            </div>
        </div>
        <div id="footer">
            <div id="footerBox">
            	<div class="footerBox_left"></div>
              	<div class="footerBox_body">
                    <div class="under18"></div>
                    <div class="rules" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Rules &amp; Mechanics</div>
                    <div class="terms" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'">Terms &amp; Conditions</div>
                </div>
                <div class="footerBox_right"></div>
            </div>
         </div>
</body>
</html>
