<?php
//ini_set('display_errors', 1);
//ini_set('log_errors', 1);
session_start();
include('controller/trans.php');
include('controller/clobby.php');
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="css/launchpad817.css" />
<link rel="stylesheet" type="text/css" href="css/style817.css" />
<link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
<link href="css/lightbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="js/trans.js"></script>
<script language="javascript" type="text/javascript" src="js/CSSPopUp.js"></script>
<script language="javascript" type="text/javascript" src="js/convert.js"></script>
<script src="js/jquery-1.4.1.js" type="text/javascript"></script>
<script src="js/jquery.background.image.scale-0.1.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="js/lightbox.js"></script>


<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/easySlider817.js"></script>
<script type="text/javascript" src="js/easySlider817_1.js"></script>

<script type="text/javascript">
       //Using document.ready causes issues with Safari when the page loads
        jQuery(window).load(function(){
                $("#contentContainer").backgroundScale({
                        imageSelector: "#gaBG",
                        centerAlign: true,
                        containerPadding: 0
                });
        });
</script>
<script type="text/javascript">
	var next = 'NEXT1';
        var prev = 'PREVIOUS1';
        $(document).ready(function(){	
		$("#slider").easySlider({
			prevText: prev,
			nextText: next,		
			lastShow: false,
			vertical: true
//                        continuous: true
                      
		});
                $("#slider1").Slider({
                	
			lastShow: false,
			vertical: true
//                         continuous: true
		});
              
              
	});	
</script>

<title>Games List</title>
<?php $xajax->printJavascript(); ?>

</head>
    <style type="text/css">

html {height:100%;}
body {
height:100%;
}

</style>
<style>
/* Easy Slider */

	#slider ul, #slider li{
		margin:0;
		padding:0;
		list-style:none;
		}
	#slider li{ 
		width:800px;
		height:300px;
		overflow:hidden; 
		}
             
/* // Easy Slider */
#slider1 ul, #slider1 li{
		margin:0;
		padding:0;
		list-style:none;
		}
	#slider1 li{ 
		width:800px;
		height:300px;
		overflow:hidden; 
		}

</style>
<!--<body onload="//do_getbalance();">-->
<div id="blanket" style="display:none;"></div>
<div id="popUpDivConvert" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 180px;">CONVERT POINTS</div></b></div>
    <div id="popup_container_home">
        <div id="convert" align="center"></div><div id="convert_img" align="center" style="visibility:hidden;"><img src="images/load_bal.gif" height="20px" /></div>
        <div id="okbtn" align="center" style="margin-top: 0px;"></div><div id="okbtn_img" align="center" style="margin-top: -20px; visibility:hidden;">PROCESSING</div>
    </div>
</div>

<div id="popUpDivLPConvert" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 100px;">CONVERT POINTS CONFIRMATION</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="convert" align="center"></div><div id="convert_img" align="center"><p>Are you sure you want to convert your <label id="convbal"></label></p><br/> <p> point/s and end your gaming session now?</p></div>
        <div id="okbtn" align="center" style="margin-top: 40px; margin-left: 80px; float: left;"><img src="images/OK Button.png" alt="" onclick="convert_points(); popup('popUpDivLPConvert');" style="cursor:pointer;"/></div><div style="margin-top: 35px;"><img src="images/cancelbutton.png" alt="" onclick="popup('popUpDivLPConvert');" style="cursor:pointer;"/></div>
    </div>
</div>

<div id="popUpDivLPCheckActiveSession" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 230px;">ALERT</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="msg" align="center"></div>
        <div id="okbtn" align="center" style="margin-top: 20px;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivLPCheckActiveSession');" style="cursor:pointer;"/></div>
    </div>
</div>

<div id="light" class="white_content"><?php include('mechanics.php') ?></div>
<div id="light2" class="white_content"><?php include('terms.php') ?></div>
<div id="light3" class="white_content2"><div align="center"><br/><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 30px;" /></div></div>
<div id="fade" class="black_overlay"></div>

 
    <div style="height:100%;width:100%;overflow: hidden;display:block;margin:0;">
        

    <div style="background: url(images/header.png) #cdee10 repeat-x;
	top: 0;width:100%;height:28%;margin:0 0 0 0;">
            <table width="100%" border="0">
                <tr>
                    <td align="center">
                        <div style="width:100%;height:100%;margin: 0;float: left;">
                       <img src="images/theSweepsLogo.png" alt="" style="height:100%;width:100%;" />
                        </div>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                       
                    
                    <td align="center">
                        <div style="color: white;">Logged In As:
                            <?php
                            $login = $_SESSION['user'];
                            $new_string = ereg_replace("[^0-9]", "", $login);

                            echo "Terminal ".$new_string;
                            ?>
                        </div>
                            
<!--                            box-->
<div id="" style="width:100%;height: 5.5em;margin: 8% 0 0 0%;float:left;">
  <div class="" style="background:url(images/pointContainer_left.png) 0 0 no-repeat;width:0.625em;height: 100%;margin: 0 0 0 0;float:left;"></div>
          
   <div class="" style="background:url(images/pointContainer_body.png) 0 0 repeat-x;width:80%;height: 100%;margin: 0 0 0 0;float:left;">
                 <img src="images/load_bal.gif" id="load_bal_img" alt="" style="margin-left: .1%; margin-top: 1%; width: 60%;" /><div id="balance"></div></div>
     <div class="" style="background:url(images/pointContainer_right.png) 0 0 no-repeat;width:10%;height: 300%;margin: 0 0 0 0;float:left;"></div>
		       
</div>


<div id="" style="width:100%;height: 180%;margin: 0 0 0 0%;float:left;">
 <div class="" style="margin: 0 0 0 0;float:left;cursor: pointer;" onclick="show_loading(); xajax_GetBalanceConv();">
                                    <img src="images/convertPoints.png" height="33px" width="150px" />
                                </div>
                                <?php echo $conv_btn ?>
                                <div class=""  style="margin: 0 0 0 1%;float:left;cursor: pointer;" onclick="xajax_CheckSessionSweepsCode();">
                                    <img src="images/enterCode.png" height="33px" width="160px" />
                                </div>
		        </div>
                    </td>
                        
                        
<!--                        gif-->
                    <td align="center">
                        <div id="" style="margin: 0% 0 0 3%;">
                          <div id="" style="margin: 0% 0 0 3%;width: 90%;height: 100%">
                               <a href=""><img src="images/5000.gif" height="155px" width="245px" style="margin-top: 30px; margin-right: 2px;"/></a>
                          </div>
                            <div id="" style="margin: 0% 0 0 3%;width: 100%;height: 100%;float: left;">
                                <img src="images/adContainer.png" height="170px" width="265px" style="margin-top: -160px; margin-right: 12px;"/>
                            </div>
                        </div>
                    </td>
                </tr>

            </table>
        </div>
        
        
 </div>

<!--
         <div id="contentContainer">
            <img id="gaBG" src="" />
			<div style="position: absolute; width:100px;margin-left: 788px;height:50px;margin-top:205px">
				<div class="back_launchPad" onclick="location.href='launchpad.php';"></div>		
			</div>
           
            <iframe src="03.php" style="width:2000px;height:900px; border: none;margin-top: 50px;">
            </iframe> 
  <div id="slider1" style="margin-left:8px;margin-top:55px;">
		<ul style="margin-left:0px;margin-top:0px;">				
			<li style="margin-left:0px;margin-top:0px;height: 370px;">
                            <div style="margin-left:0px;margin-top:170px;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/kenowithBG.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>KENO</p></div>
                            </div>
                             <div style="margin-left:200px;margin-top:-170px;" class="horseRacing" onclick="check_session('qhorse1');">
					<img src="images/horseracingwithBG1.png" height="150px" width="180px"/>
					<div class="gameTitles">QUARTER HORSE RACING</div>
                            </div>
                            <div style="margin-left:400px;margin-top:-169px;" class="horseRacing" onclick="check_session('bj');">
					<img src="images/BJBG.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>BLACK JACK</p></div>
                            </div>
                            <div style="margin-left:600px;margin-top:-168px;" class="horseRacing" onclick="check_session('wheel');">
				<img src="images/WheelofWealth1.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>WHEEL OF WEALTH</p></div>	
                               
                            </div>
                         </li>
                      <li style="margin-left:0px;margin-top:0px;height: 600px;">
                            <div style="margin-left:0px;margin-top:160px;" class="horseRacing" onclick="check_session('bigkahuna');">
					<img src="images/BigKahuna.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>BIG KAHUNA SHAKE & LADDERS</p></div>
                            </div>
                             <div style="margin-left:200px;margin-top:-168px;" class="horseRacing" onclick="check_session('goldfactory');">
                                        <img src="images/GoldFactory.png" height="150px" width="180px" />
					<div class="gameTitles"><p>GOLD FACTORY</p></div>
                            </div>
                            <div style="margin-left:400px;margin-top:-168px;" class="horseRacing" onclick="check_session('karatepig');">
					<img src="images/KaratePig.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>KARATE PIG</p></div>
                            </div>
                            <div style="margin-left:600px;margin-top:-167px;" class="horseRacing" onclick="check_session('surewin');">
					<img src="images/SureWin.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>SURE WIN</p></div>
                            </div>
                         </li>
                     
               </ul>
	</div>          
            
 	<div id="slider" style="margin-left:8px;">
		<ul>				
			
                    <li style="margin-left:0px;margin-top:0px;height: 200px;">
                              <div style="margin-left:0px;margin-top:0px;" class="horseRacing" onclick="check_session('bigkahuna');">
					<img src="images/BigKahuna.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>BIG KAHUNA SHAKE & LADDERS</p></div>
                            </div>
                         <div style="margin-left:200px;margin-top:-166px;" class="horseRacing" onclick="check_session('goldfactory');">
                                        <img src="images/GoldFactory.png" height="150px" width="180px" />
					<div class="gameTitles"><p>GOLD FACTORY</p></div>
                            </div>
                          <div style="margin-left:400px;margin-top:-166px;" class="horseRacing" onclick="check_session('karatepig');">
					<img src="images/KaratePig.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>KARATE PIG</p></div>
                            </div>
                            <div style="margin-left:600px;margin-top:-167px;" class="horseRacing" onclick="check_session('surewin');">
					 <img src="images/SureWin.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>SURE WIN</p></div>
                            </div>
                
			</li>
                    
                 <li style="margin-left:0px;margin-top:0px;height: 200px;">
                              <div style="margin-left:0px;margin-top:0px;" class="horseRacing" onclick="check_session('tomb2');">
					<img src="images/TombRaider2BG.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>TOMB RAIDER: SECRET OF THE SWORD</p></div>
                            </div>
                         <div style="margin-left:200px;margin-top:-177px;" class="horseRacing" onclick="check_session('thunderstruck2');">
                                        	<img src="images/ThunderStruckII.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>THUNDER STRUCK II</p></div>
                            </div>
                          <div style="margin-left:400px;margin-top:-165px;" class="horseRacing" onclick="check_session('tomb');">
					<img src="images/TombRaider.png" height="150px" width="180px"/>
					<div class="gameTitles"><p>TOMB RAIDER</p></div>
                            </div>
                            <div style="margin-left:600px;margin-top:-167px;" class="horseRacing" onclick="check_session('dino');">
                                         <img src="images/dino.png" height="150px" width="180px"/>
					<div class="gameTitles">DINO MIGHT</div>
                            </div>
                
			</li> 
                       
               </ul>
	</div>-->
            
      
       <div id="" style=" background: url(images/footer.png) #d6cec3 repeat-x;
        position: absolute;bottom: 0;width:100%;height:110px;margin:0;z-index: 2;">
           
           <div id="" style="width:735px;height:75px;right: auto;display: block;margin: 20px auto;">
               
                <div class="" style="background: url(images/footerContBox_left.png) 0 0 no-repeat;
	width:14px;height:76px;margin:0 0 0 0;float:left;"></div>
                
                
                <div class="" style="background: url(images/footerContBox_body.png) 0 0 repeat-x;
	width:707px;height:76px;margin:0 0 0 0;float:left;">
                   
                    
                    <div class="" style="background: url(images/under18.png) 0 0 no-repeat;
	width:45px;height:44px;margin: 16px 0 0 64px;float:left;"></div>
                   
                    
                    <div class="" style="font: 16px/20px 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;
	color:#FFF;text-decoration:none;width:156px;height:20px;margin: 30px 0 0 131px;float:left;cursor: pointer;" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Rules &amp; Mechanics</div>
                   
                    
                    <div class="" style="font: 16px/20px 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;
	color:#FFF;text-decoration:none;width:165px;height:20px;margin: 30px 0 0 100px;	float:left;cursor: pointer;" 
        onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'">Terms &amp; Conditions</div>
                </div>
                <div class="" style="background: url(images/footerContBox_right.png) 0 0 no-repeat;
	width:14px;height:76px;	margin:0 0 0 0;	float:left;"></div>
            </div>
         </div>

<!--    </div>-->
</body>
</html>
