<?php
//ini_set('display_errors', 1);
//ini_set('log_errors', 1);
session_start();
include('controller/trans.php');
include('controller/clobby.php');
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="css/launchpad.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
<link href="css/lightbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="js/trans.js"></script>
<script language="javascript" type="text/javascript" src="js/CSSPopUp.js"></script>
<script language="javascript" type="text/javascript" src="js/convert.js"></script>
<script src="js/jquery-1.4.1.js" type="text/javascript"></script>
<script src="js/jquery.background.image.scale-0.1.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="js/lightbox.js"></script>





<link rel="stylesheet" type="text/css" href="css/lobby1.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/easySlider1.5.js"></script>
<script type="text/javascript" src="js/easySlider1.5_1.js"></script>
<style type="text/css">

html {height:100%;}
body {height:100%;}

</style>
<script type="text/javascript">
       //Using document.ready causes issues with Safari when the page loads
        jQuery(window).load(function(){
                $("#contentContainer2").backgroundScale({
                        imageSelector: "#gaBG2",
                        centerAlign: true,
                        containerPadding: 0
                });
        });
</script>
<script type="text/javascript">
	var next = 'NEXT1';
        var prev = 'PREVIOUS1';
        $(document).ready(function(){	
		$("#slider").easySlider({
			prevText: prev,
			nextText: next,		
			lastShow: false,
			vertical: true
//                        continuous: true
                      
		});
                $("#slider1").Slider({
                	
			lastShow: false,
			vertical: true
//                         continuous: true
		});
              
              
	});	
</script>



<title>Games List</title>
<?php $xajax->printJavascript(); ?>
</head>
    
<body>
    <!--<body onload="do_getbalance();">-->
<div id="blanket" style="display:none;"></div>
<div id="popUpDivConvert" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 180px;">CONVERT POINTS</div></b></div>
    <div id="popup_container_home">
        <div id="convert" align="center"></div><div id="convert_img" align="center" style="visibility:hidden;"><img src="images/load_bal.gif" height="20px" /></div>
        <div id="okbtn" align="center" style="margin-top: 0px;"></div><div id="okbtn_img" align="center" style="margin-top: -20px; visibility:hidden;">PROCESSING</div>
    </div>
</div>

<div id="popUpDivLPConvert" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 100px;">CONVERT POINTS CONFIRMATION</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="convert" align="center"></div><div id="convert_img" align="center"><p>Are you sure you want to convert your <label id="convbal"></label></p><br/> <p> point/s and end your gaming session now?</p></div>
        <div id="okbtn" align="center" style="margin-top: 40px; margin-left: 80px; float: left;"><img src="images/OK Button.png" alt="" onclick="convert_points(); popup('popUpDivLPConvert');" style="cursor:pointer;"/></div><div style="margin-top: 35px;"><img src="images/cancelbutton.png" alt="" onclick="popup('popUpDivLPConvert');" style="cursor:pointer;"/></div>
    </div>
</div>

<div id="popUpDivLPCheckActiveSession" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 230px;">ALERT</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="msg" align="center"></div>
        <div id="okbtn" align="center" style="margin-top: 20px;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivLPCheckActiveSession');" style="cursor:pointer;"/></div>
    </div>
</div>

<div id="light" class="white_content"><?php include('mechanics.php') ?></div>
<div id="light2" class="white_content"><?php include('terms.php') ?></div>
<div id="light3" class="white_content2"><div align="center"><br/><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 30px;" /></div></div>
<div id="fade" class="black_overlay"></div>



        
        
        
        
        
        
        
        
        
      
        <div style="position: absolute;top: 0;background: url(images/header.png) #d6cec3 repeat-x;
        width:100%;height:25%;margin:0 0 0 0;">
             
        
         <table border="0">
             <tr>
            <td>
           <img class="big" src="images/theSweepsLogo.png" style="position: absolute"height="190" width="270" /><br />
            </td>
 <td align="center">
        <div style="position: absolute;font:normal 16px/18px  'Lucida Sans Unicode', 'Lucida Grande', 
                  sans-serif;color: white;margin: 1% 0 0 50%;">
                 Logged In As:<?php
                            $login = $_SESSION['user'];
                            $new_string = ereg_replace("[^0-9]", "", $login);
                            echo "Terminal ".$new_string;
                            ?>
                        </div>
         <div style="position: absolute;height:100%;width:100%;margin: 4% 0 0 -15%;">
         <img class="big3"src="images/pointContainer_left.png" 
             height="20px" width="50px" />
         </div>

          <div class="big4" style="background:url(images/pointContainer_body.png) 0 0 repeat-x;position: absolute;height:100%;width:1%;margin: 4% 0 0 35.5%;height:25%;width:18%">
          <img src="images/load_bal.gif" id="load_bal_img" alt="" style="margin-left: 20px; margin-top: 10px; width: 180px;" />
                        <div id="balance"></div>
         </div>
          
           <div style="position: absolute;height:100%;width:100%;margin: 4% 0 0 4%;">
         <img class="big3"src="images/pointContainer_right.png" 
             height="20px" width="50px" />
         </div>
          
        <div style="width:100%;height: 100%;margin: 0% 0 0 0%;float:left;">
                <div style="margin: 9% 0% 0 34%;position: absolute;cursor: pointer;" onclick="show_loading(); xajax_GetBalanceConv();">
                    <img src="images/convertPoints.png" height="33px" width="130px" />
                </div> <?php echo $conv_btn ?>
                    <div style="margin: 9% 0% 0 44%;position: absolute;cursor: pointer;">
                        <img src="images/enterCode.png" height="33px" width="140px" />
                    </div>
                </div>
                        
                        
  </td>
<td align="center">
            <div style="margin: 12% 0 0 70%;position: absolute;">
            <img src="images/adContainer.png" height="170px" width="255px" style="margin-top: -180px; margin-right: -30px;"/> 
            </div> 
        <div style="margin: 0% 0 0% 70%;position: absolute; ">
        <a href=""><img src="images/5000.gif" height="130px" width="235px" style="margin: 10% 0 0% 5%"/></a>    
        </div> 
  </td>
                
</tr>
  </table>
</div>  
</div>
    //body

launch button
<div style="position: absolute; width:100%;">  
    -42.5% 0 0 80%
    <div class="back_launchPad" style="width:18%;height:36px;z-index: 2;position: absolute;margin-top: -45%;margin-left: 80%;cursor: pointer;">
    </div>
</div>
    
 
slider
    
  <div style="width:90%;height:63.5%;margin-top: -46%;">
     
      <div  id="slider1" style="width:90%;height:64.6%;margin-top: 5%;">
          <ul style="margin-left:0px;margin-top:0px;height: 70%;width: auto;">
          <li style="margin-left:0px;margin-top:0px;height: 100%;width: 100%;">
                   <div style="margin-left:.1%;margin-top:.1%;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/kenowithBG.png" height="150px" width="180px"/>
					<div class="gameTitles1"><p>KENO</p></div>
                   </div>
                  <div style="margin-left:12%;margin-top:-9.7%;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/horseracingwithBG1.png" height="150px" width="180px"/>
					<div class="gameTitles1">QUARTER HORSE RACING</div>
                   </div>
                   <div style="margin-left:25%;margin-top:-8%;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/dino.png" height="150px" width="180px"/>
					<div class="gameTitles1">DINO MIGHT</div>
                   </div>
                   <div style="margin-left:38%;margin-top:-8%;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/TombRaider.png" height="150px" width="180px"/>
				<div class="gameTitles1"><p>TOMB RAIDER</p></div>
                   </div>
              </li>
              <li style="margin-left:0px;margin-top:0px;height: 100%;width: 100%;">
                   <div style="margin-left:.1%;margin-top:3%;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/BJBG.png" height="150px" width="180px"/>
					<div class="gameTitles1"><p>BLACK JACK</p></div>
                   </div>
                  <div style="margin-left:12%;margin-top:-9.7%" class="horseRacing" onclick="check_session('keno');">
					<img src="images/TombRaider2BG.png" height="150px" width="180px"/>
				<div class="gameTitles1"><p>TOMB RAIDER: <br>SECRET OF THE SWORD</p></div>
                   </div>
                   <div style="margin-left:25%;margin-top:-10.5%;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/WheelofWealth1.png" height="150px" width="180px"/>
					<div class="gameTitles1"><p>WHEEL OF WEALTH</p></div>
                   </div>
                   <div style="margin-left:38%;margin-top:-9.5%;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/BigKahuna.png" height="150px" width="180px"/>
					<div class="gameTitles1"><p>BIG KAHUNA</p></div>
                   </div>
              </li>
          </ul>
  </div>
      
      <div  id="slider"style="width:90%;height:60%;margin-top: 1%;">
          <ul style="margin-left:0px;margin-top:0px;height: 70%;">
               <li style="margin-left:0px;margin-top:0px;height: 100%;width: 90%;">
                   <div style="margin-left:.1%;margin-top:-.1%;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/BJBG.png" height="150px" width="180px"/>
					<div class="gameTitles1"><p>BLACK JACK</p></div>
                   </div>
                  <div style="margin-left:14.8%;margin-top:-11.9%;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/TombRaider2BG.png" height="150px" width="180px"/>
					<div class="gameTitles11"><p>TOMB RAIDER: <br>SECRET OF THE SWORD</p></div>
                   </div>
                   <div style="margin-left:31%;margin-top:-11.7%;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/WheelofWealth1.png" height="150px" width="180px"/>
					<div class="gameTitles1"><p>WHEEL OF WEALTH</p></div>
                   </div>
                   <div style="margin-left:73%;margin-top:-19%;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/BigKahuna.png" height="150px" width="180px"/>
					<div class="gameTitles1"><p>BIG KAHUNA</p></div>
                   </div>
              </li>
              
                   <li></li> 
                   
             <li>
                   <div style="margin-left:.1%;margin-top:.1%;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/GoldFactory.png" height="150px" width="180px"/>
					<div class="gameTitles1"><p>GOLD FACTORY1</p></div>
                   </div>
                  <div style="margin-left:23%;margin-top:-19%;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/KaratePig.png" height="150px" width="180px"/>
					<div class="gameTitles1"><p>KARATE PIG</p></div>
                   </div>
                   <div style="margin-left:48%;margin-top:-19%;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/SureWin.png" height="150px" width="180px"/>
					<div class="gameTitles1"><p>SURE WIN</p></div>
                   </div>
                   <div style="margin-left:73%;margin-top:-19%;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/ThunderStruckII.png" height="150px" width="180px"/>
					<div class="gameTitles1"><p>THUNDER STRUCK II</p></div>
                   </div>
              </li>
          </ul> 
  </div>
</div>
      
 


 <div style="position: absolute; width:100%;height: 100%;">
    <div style="background: url(images/kenowithBG.png) #d6cec3 repeat-X;
         position: absolute;width:100%;height:100%;margin: -40.5% 0 0 0%;">
    </div>
     <div style="background: url(images/kenowithBG.png) #d6cec3 repeat-X;
         position: absolute;width:100%;height:100%;margin: -20.5% 0 0 0%;">
    </div>
</div>
             
  <div id="slider1" style="margin: 10% 0 0 10%;width: auto;height: auto;">
		<ul style="margin-left:0px;margin-top:0px;">				
			<li style="margin-left:0px;margin-top:0px;height: 0px;">
                            <div style="margin-left:0px;margin-top:0px;" class="horseRacing" onclick="check_session('keno');">
					<img src="images/kenowithBG.png" height="150px" width="180px"/>
					<div class="gameTitles1"><p>KENO</p></div>
                            </div>
                         </li>
                     
                     
               </ul>
	</div> 








    
    
    
    
    
    
 footer   
      <div style="background: url(images/footer.png) #d6cec3 repeat-x;position: absolute;bottom: 0;width:100%;
           height:10%;margin:0;z-index: 2;">
          
          
          <div style="width:735px;height:75px;	
        right: auto;display: block;margin: 20px auto;">
              foot left
              <div class="" style="background: url(images/footerContBox_left.png) 0 0 no-repeat;
	width:14px;height:76px;	margin:0 0 0 0;	float:left;"></div>
 body foot               
              <div class="" style="background: url(images/footerContBox_body.png) 0 0 repeat-x;
	width:707px;height:76px;margin:0 0 0 0;float:left;">
          
                    <div class="" style="background: url(images/under18.png) 0 0 no-repeat;
	width:45px;height:44px;margin: 16px 0 0 64px;float:left;"></div>
            
       <div class=""
                  style="font: 16px/20px 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;
	color:#FFF;text-decoration:none;width:156px;height:20px;margin: 30px 0 0 131px;float:left;cursor: pointer;"
                         onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Rules &amp; Mechanics</div>
               
                  <div class="" style ="font: 16px/20px 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;
	color:#FFF;text-decoration:none;width:165px;height:20px;margin: 30px 0 0 100px;float:left;cursor: pointer;"
                       onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'">Terms &amp; Conditions</div>
                </div>
              foot right
                <div class="" style="background: url(images/footerContBox_right.png) 0 0 no-repeat;
	width:14px;height:76px;margin:0 0 0 0;float:left;"></div>
            </div>
          
          
          
      </div>
    
</body>
    
</html>
