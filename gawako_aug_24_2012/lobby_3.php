<?php
//ini_set('display_errors', 1);
//ini_set('log_errors', 1);
session_start();
include('controller/trans.php');
include('controller/clobby.php');
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<link rel="stylesheet" type="text/css" href="css/lobby.css" />
<link rel="stylesheet" type="text/css" href="css/launchpad.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
<link href="css/lightbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="js/trans.js"></script>
<script language="javascript" type="text/javascript" src="js/CSSPopUp.js"></script>
<script language="javascript" type="text/javascript" src="js/convert.js"></script>
<script src="js/jquery-1.4.1.js" type="text/javascript"></script>
<script src="js/jquery.background.image.scale-0.1.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="js/lightbox.js"></script>


<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/easySlider1.5.js"></script>
<script type="text/javascript" src="js/easySlider1.5_1.js"></script>

<script type="text/javascript">
       //Using document.ready causes issues with Safari when the page loads
        jQuery(window).load(function(){
                $("#contentContainer").backgroundScale({
                        imageSelector: "#gaBG",
                        centerAlign: true,
                        containerPadding: 0
                });
        });
</script>
<script type="text/javascript">
	var next = 'NEXT1';
        var prev = 'PREVIOUS1';
        $(document).ready(function(){	
		$("#slider").easySlider({
			prevText: prev,
			nextText: next,		
			lastShow: false,
			vertical: true
//                        continuous: true
                      
		});
                $("#slider1").Slider({
                	
			lastShow: false,
			vertical: true
//                         continuous: true
		});
              
              
	});	
</script>

<title>Games List</title>
<?php $xajax->printJavascript(); ?>

</head>
<style>
/* Easy Slider */

	#slider ul, #slider li{
		margin:0;
		padding:0;
		list-style:none;
		}
	#slider li{ 
		width:800px;
		height:300px;
		overflow:hidden; 
		}
             
/* // Easy Slider */
#slider1 ul, #slider1 li{
		margin:0;
		padding:0;
		list-style:none;
		}
	#slider1 li{ 
		width:800px;
		height:300px;
		overflow:hidden; 
		}

</style>
<!--<body onload="do_getbalance();">-->
<div id="blanket" style="display:none;"></div>
<div id="popUpDivConvert" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 180px;">CONVERT POINTS</div></b></div>
    <div id="popup_container_home">
        <div id="convert" align="center"></div><div id="convert_img" align="center" style="visibility:hidden;"><img src="images/load_bal.gif" height="20px" /></div>
        <div id="okbtn" align="center" style="margin-top: 0px;"></div><div id="okbtn_img" align="center" style="margin-top: -20px; visibility:hidden;">PROCESSING</div>
    </div>
</div>

<div id="popUpDivLPConvert" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 100px;">CONVERT POINTS CONFIRMATION</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="convert" align="center"></div><div id="convert_img" align="center"><p>Are you sure you want to convert your <label id="convbal"></label></p><br/> <p> point/s and end your gaming session now?</p></div>
        <div id="okbtn" align="center" style="margin-top: 40px; margin-left: 80px; float: left;"><img src="images/OK Button.png" alt="" onclick="convert_points(); popup('popUpDivLPConvert');" style="cursor:pointer;"/></div><div style="margin-top: 35px;"><img src="images/cancelbutton.png" alt="" onclick="popup('popUpDivLPConvert');" style="cursor:pointer;"/></div>
    </div>
</div>

<div id="popUpDivLPCheckActiveSession" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 230px;">ALERT</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="msg" align="center"></div>
        <div id="okbtn" align="center" style="margin-top: 20px;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivLPCheckActiveSession');" style="cursor:pointer;"/></div>
    </div>
</div>

<div id="light" class="white_content"><?php include('mechanics.php') ?></div>
<div id="light2" class="white_content"><?php include('terms.php') ?></div>
<div id="light3" class="white_content2"><div align="center"><br/><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 30px;" /></div></div>
<div id="fade" class="black_overlay"></div>

    <div id="mainContainer1">
    	<div id="banner1">
            <div id="logo3">
                        <img src="images/theSweepsLogo1.png" alt="" />
                        <div style="color: white;margin-left: 50%;margin-top: -9%; z-index: 1;">Logged In As:
                            <?php
                            $login = $_SESSION['user'];
                            $new_string = ereg_replace("[^0-9]", "", $login);
                            echo "Terminal ".$new_string;
                            ?>
                        </div>
		    	<div id="txtBoxContainer_point1">
                            <div class="txtBox_left1"></div>
                            <div class="txtBox_body1">
                                <img src="images/load_bal.gif" id="1load_bal_img" alt="" style="margin-left: 20px; margin-top: 10px; width: 150px;" />
                                <div id="balance1"></div></div>
		            <div class="txtBox_right1"></div>
		        </div>
		        <div id="btnContainer1">
		        	<div class="convertPoints1" onclick="show_loading(); xajax_GetBalanceConv();">
                                    <img src="images/convertPoints.png" height="33px" width="140px" />
                                </div>
                                <?php echo $conv_btn ?>
                                <div class="enterCode1" onclick="xajax_CheckSessionSweepsCode();">
                                    <img src="images/enterCode.png" height="33px" width="150px" />
                                </div>
		        </div>
                        <div id="adContainer1">
                            <a href="">
                            <img src="images/5000.gif" height="155px" width="245px" style="margin-top: 30px; margin-right: 2px;"/></a>
                            <div class="adContent1">
                                <img src="images/adContainer.png" height="170px" width="265px" style="margin-top: -160px; margin-right: 12px;"/>
                            </div>
                        </div>

                            </div>
        </div>
        
        
 </div>

      
        <div id="footer1">
            <div id="footerBox1">
                <div class="footerBox_left1"></div>
                <div class="footerBox_body1">
                    <div class="under181"></div>
                    <div class="rules1" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Rules &amp; Mechanics</div>
                    <div class="terms1" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'">Terms &amp; Conditions</div>
                </div>
                <div class="footerBox_right1"></div>
            </div>
         </div>
</body>
</html>
