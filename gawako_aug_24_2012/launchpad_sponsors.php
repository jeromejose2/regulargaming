<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Launch Pad</title>
<link rel="stylesheet" type="text/css" href="css/main2.css" />
</head>

<body>
	<div id="mainContainer">
    	<div id="banner">
        	<div id="logo"> <img src="images/theSweepsLogo.png" /></div>
            <div id="points"> 
            	<div id="txtBoxContainer_point">
                	<div class="txtBox_left"></div>
                    <div class="txtBox_body"> <p>Point Balance: </p></div>
                    <div class="txtBox_right"></div>
                </div>
                <div id="btnContainer">
                	<a href="eSweeps-page.html"><div class="convertPoints"></div></a>
                    <a href="entrycode-page.html"><div class="enterCode"></div></a>
                </div>
            </div>
            <div id="rotatingAd">
            	<div id="adContainer">
                	<a href=""><div class="adContent"></div></a>
                </div>
            </div>
		</div>
        <div id="contentContainer">
        	<div id="launch_Btn">
            	<a href="gameList-page.html"><div class="launch_game"></div></a><br />
                <div class="launch_title"> Play<br /> Games</div>
            </div>
            <div id="launch_Btn2">
            	<a href=""><div class="launch_game"></div></a><br />
                <div class="launch_title"> Browse<br /> Internet</div>
            </div>
        </div>
        <div id="footer">
        	<div id="sponsorBox">
            	<div class="sponsorBox_left"></div>
              	<div class="sponsorBox_body">
           	  		<div class="sponsor_logo"></div>
           	  		<div class="sponsor_ad"></div>
           	  		<div class="sponsor_logo"></div>
              	</div>
                <div class="sponsorBox_right"></div>
            </div>
        	
        	<div id="footerBox">
            	<div class="footerBox_left"></div>
              	<div class="footerBox_body">
           	  		<div class="under18"></div>
                    <div class="rules">	<a href="">Rules &amp; Mechanics</a></div>
                    <div class="terms"> <a href="">Terms &amp; Conditions</a></div>
              	</div>
                <div class="footerBox_right"></div>
            </div>
         </div>
    </div>
</body>
</html>
