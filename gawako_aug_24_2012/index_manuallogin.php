<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('log_errors', 1);
session_start();
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login : Launch Pad</title>
<link rel="stylesheet" type="text/css" href="css/login.css" />
<link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="js/CSSPopUp.js"></script>
<script language="javascript" type="text/javascript" src="js/form_validation.js"></script>
</head>

<body>
<div id="mainContainer">
<div id="blanket" style="display:none;"></div>
<div id="popUpDivLoginUname" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#139E9E; background-color: #77A6A0; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 200px;">INVALID LOG IN</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="convert" align="center"></div><div id="convert_img" align="center">Please enter your username.</div><!-- <div id="convert_img" align="center">Please enter a username.</div> -->
        <div id="okbtn" align="center" style="margin-top: 20px;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivLoginUname');" style="cursor:pointer;"/></div>
    </div>
</div>
<div id="popUpDivLoginPass" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#139E9E; background-color: #77A6A0; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 200px;">INVALID LOG IN</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="convert" align="center"></div><div id="convert_img" align="center">Please enter your password.</div><!-- <div id="convert_img" align="center">Please enter a password.</div> -->
        <div id="okbtn" align="center" style="margin-top: 20px;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivLoginPass');" style="cursor:pointer;"/></div>
    </div>
</div>

    <div id="banner"></div>
    <div id="contentContainer">
        <div id="Login_Cont">
            <div class="login_logo"></div>
            <div class="login_body">
                <form method="post" action="controller/checklogin.php" onsubmit="return checkform2(this);">
                <div class="loginCont_body">
                    <div class="txtName">USERNAME</div>
                    <div class="inputbox">
                         <input type="text" name="txtuser" id="textfield2" class="inputBoxEffect" />
                     </div>
                     <div class="txtName">PASSWORD</div>
                     <div class="inputbox">
                         <input type="password" name="txtpass" id="textfield2" class="inputBoxEffect" />
                     </div>
                     <div class="loginCont_btn">
                        <input type="submit" value="" class="login" />
                    </div>
                </div>
                </form>
                <div class="login_bottom">&nbsp;</div>
                <div id="err_msg" align="center"><?php include('controller/cindex.php'); ?></div>
            </div>
        </div>
        <div id="footer"></div>
    </div>
</div>
</body>
</html>
